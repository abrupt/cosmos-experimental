# ~/ABRÜPT/FRÉDÉRIC NEYRAT/COSMOS EXPÉRIMENTAL/*

La [page de ce livre](https://abrupt.cc/frederic-neyrat/cosmos-experimental/) sur le réseau.

## Sur le livre

Le cosmos n’est pas un espace dans lequel seraient contenus des objets (planètes, soleils, trous noirs), soutient l’auteur, mais une expérience en cours --- une expansion vers l’inconnu. Pour appréhender cette expérience cosmologique, la philosophie doit changer de forme, de technique démonstrative, et devenir ce qu’outre-Atlantique on nomme théorie-fiction. La théorie-fiction ne mélange pas les genres, mais resculpte leur ligne de force : l’imaginaire réactive des concepts fossilisés et les concepts orientent l’imaginaire. Composé de fragments, d’aphorismes, de récits et d’enquêtes métaphysiques, *Cosmos expérimental* propose un voyage dans l’espace-temps où des personnages énigmatiques échangent sur l’astrophysique, la technologie, la musique, les anges, le communisme, la mort, et l’éternité.

*[Lionel Marchetti](https://lionelmarchetti.bandcamp.com/) a également composé Icare Noir, une œuvre musicale pour accompagner Cosmos Expérimental.*

## Sur l'auteur

Frédéric Neyrat est philosophe, explorant par l'écriture et les images le territoire de ce qu'il nomme un "existentialisme radical". Il est professeur associé dans le Département d'Anglais de l'Université Wisconsin-Madison (États-Unis), dans laquelle il a également une chaire d'"humanités planétaires". Il anime la plateforme électronique *Alienocene* et a récemment publié *L'Ange Noir de l'Histoire : Cosmos et technique de l'Afrofuturisme* (éditions MF, 2021), *Literature and Materialisms* (Routledge, 2020), *Échapper à l'horreur: Traité des interruptions merveilleuses* (Lignes, 2017) et *La Part inconstructible de la Terre : Critique du géo-constructivisme* (Seuil, 2016). Sites : [Atopies](https://atoposophie.wordpress.com/).

## Sur la licence

Cet [antilivre](https://abrupt.cc/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.cc) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.cc/partage/).
