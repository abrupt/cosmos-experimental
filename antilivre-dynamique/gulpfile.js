// Include Gulp
import gulp from 'gulp';
import browserSync from 'browser-sync';
import critical from 'critical';
import del from 'del';
import cache from 'gulp-cache';
import gulpif from 'gulp-if';
import imageResize from 'gulp-image-resize';
import changed from 'gulp-changed';
import rename from 'gulp-rename';
import imagemin from 'gulp-imagemin';
import imageminMozjpeg from 'imagemin-mozjpeg';
import imageminGifsicle from 'imagemin-gifsicle';
import imageminSvgo from 'imagemin-svgo';
import imageminOptipng from 'imagemin-optipng';
import { exec } from 'child_process'; // For running a local machine task

// Compress and transform all images
gulp.task('images', function(done) {

  [10000,1200,800,400].forEach(function (size) {
    gulp.src( ['src/img/**/*.{png,jpg}', '!src/img/**/*.gif', '!src/img/**/*.mp3', '!src/img/**/*.mp4'])
      .pipe(changed('static/img/'))
      .pipe(imageResize({ width: size }))
      .pipe(cache(imagemin([
        imageminGifsicle({interlaced: true, optimizationLevel: 3}),
        imageminMozjpeg({quality: 70, progressive: true}),
        imageminOptipng({optimizationLevel: 5}),
        imageminSvgo({
            plugins: [
                {removeViewBox: true},
                {cleanupIDs: false}
            ]
        })
      ], {
          verbose: true
      }
      )))
      .pipe(gulpif(size == 10000, rename(function (path) { path.basename = `${path.basename}`; })))
      .pipe(gulpif(size == 1200, rename(function (path) { path.basename = `${path.basename}-large`; })))
      .pipe(gulpif(size == 800, rename(function (path) { path.basename = `${path.basename}-medium`; })))
      .pipe(gulpif(size == 400, rename(function (path) { path.basename = `${path.basename}-small`; })))
      .pipe(gulp.dest('static/img/'));
    });
  gulp.src( ['src/img/**/*.mp4','src/img/**/*.gif','src/img/**/*.mp3'])
     .pipe(gulp.dest('static/img/'));
  done();
});
// gulp.task('images', () => {
//   const f = filter(['src/img/**', '!src/img/favicon/*'], { restore: true });
//   return gulp.src('src/img/**')
//       .pipe(f)
//       .pipe(cache(imagemin([
//         imagemin.gifsicle({interlaced: true, optimizationLevel: 3}),
//         imagemin.mozjpeg({progressive: true}),
//         imagemin.optipng({optimizationLevel: 5}),
//         imagemin.svgo({
//             plugins: [
//                 {removeViewBox: true},
//                 {cleanupIDs: false}
//             ]
//         })
//        ], {verbose: true}
//        )))
//        .pipe(f.restore)
//        .pipe(gulp.dest('static/img/'));
// });

// Clean : clear cache and delete images
gulp.task('clean', function () {
  return del(['static/img/*']);
  cache.clearAll();
});

// Generate & Inline Critical-path CSS
gulp.task('critical', function (done) {
  critical.generate({
  base: 'public-dev/',
  src: 'index.html',
  css: ['public-dev/css/style.*.css'],
  target: {
    css: '../assets/css/critical.css',
  },
  dimensions: [
    {
      height: 600,
      width: 300,
    },
    {
      height: 800,
      width: 1300,
    },
  ],
});
  done();
});

// Hugo
gulp.task(
  'hugo',
  gulp.series(
  function (cb) {
  exec("hugo --minify --cleanDestinationDir --environment development", function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
  })
);

gulp.task(
  'hugo-prod',
  gulp.series(
  function (cb) {
  exec("hugo --minify --cleanDestinationDir --environment production", function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
  })
);

// Browser-sync
gulp.task('browser-sync', function(cb) {
  browserSync({
    server: {
      baseDir: "public-dev"
    },
    // https: true,
    open: false
  }, cb);
});

function reload(done) {
  browserSync.reload();
  done();
}

// Watch
gulp.task('watch', function () {
  gulp.watch([
    'content/**',
    'assets/**',
    'data/**',
    'layouts/**',
    'static/**',
    'archetypes/**',
    'config/**'
  ], gulp.series('hugo', reload));
  gulp.watch('src/img/**/*', gulp.series('images', reload));
});

gulp.task(
  'default',
  gulp.series('images', 'hugo', 'critical', 'browser-sync', 'watch')
);

gulp.task(
  'deploy',
  gulp.series('images', 'hugo-prod', 'watch')
);
