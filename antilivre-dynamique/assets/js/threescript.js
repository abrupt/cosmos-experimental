import * as THREE from './three.module.js';

import { FirstPersonControls } from './FirstPersonControls.js';
import { TrackballControls } from './TrackballControls.js';

let container;
let camera, controls, scene, renderer;

let textureEquirec;

let listSolids = [];
const textes = [...document.querySelectorAll('.texte')];
const texteContainer = document.querySelector('.textes');

function randomNb(min, max) { // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function randomNbFrac(min, max) { // min and max included
  return Math.random() * (max - min + 1) + min;
}

function isTouchDevice() {
  return (('ontouchstart' in window) ||
     (navigator.maxTouchPoints > 0) ||
     (navigator.msMaxTouchPoints > 0));
}

const mouse = new THREE.Vector2();

const clock = new THREE.Clock();

init();
animate();

function init() {

  container = document.getElementById( 'container' );

  camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 1, 20000 );

  scene = new THREE.Scene();
  // scene.background = new THREE.Color( 0xefd1b5 );

  // scene.add( new THREE.AmbientLight( 0xaaaaaa ) );
  // const light = new THREE.DirectionalLight( 0xffffff, 1 );
  // light.position.set( 1, 100, 1 ).normalize();
  // scene.add( light );
  const ambient = new THREE.AmbientLight( 0xffffff );
  scene.add( ambient );


// const axesHelper = new THREE.AxesHelper( 5000 );
// axesHelper.setColors("blue", "red", "green") 
// scene.add( axesHelper );


  const textureLoader = new THREE.TextureLoader();

  textureEquirec = textureLoader.load( './img/fond.jpg' );
  textureEquirec.mapping = THREE.EquirectangularReflectionMapping;
  textureEquirec.encoding = THREE.sRGBEncoding;

  scene.background = textureEquirec;
  // scene.background = new THREE.Color( 0xefd1b5 );
  // scene.fog = new THREE.FogExp2( 0xefd1b5, 0.00025 );

  const nbSolids = textes.length;
  const radius = 150;
  const solid = [
    new THREE.TetrahedronGeometry( radius, 0 ),
    new THREE.BoxGeometry( radius, radius, radius ),
    new THREE.OctahedronGeometry( radius, 0 ),
    new THREE.DodecahedronGeometry( radius, 0 ),
    new THREE.IcosahedronGeometry( radius, 0 )
  ];

  let distance;
  let randomScale;
  if (isTouchDevice()) {
    distance = 4000;
    randomScale = randomNbFrac(1,2);
  } else {
    distance = 5000;
    randomScale = randomNbFrac(1,3);
  }

  for ( let i = 0; i < nbSolids; i ++ ) {

    const object = new THREE.Mesh( solid[randomNb(0, 4)], new THREE.MeshPhongMaterial( { envMap: textureEquirec } ) );

    object.position.x = randomNb(-distance,distance);
    object.position.y = randomNb(-distance,distance);
    object.position.z = randomNb(-distance,distance);

    object.scale.x = randomScale;
    object.scale.y = randomScale;
    object.scale.z = randomScale;

    object.name = i;

    listSolids.push(object);
    scene.add( object );

  }

  renderer = new THREE.WebGLRenderer({ antialias: true });
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize( window.innerWidth, window.innerHeight );
  renderer.outputEncoding = THREE.sRGBEncoding;
  container.appendChild( renderer.domElement );

  if (isTouchDevice()) {
    camera.position.set( -1000, -1000, -1000 );
    controls = new TrackballControls( camera, renderer.domElement );
    controls.rotateSpeed = 1.0;
    controls.zoomSpeed = 1.2;
    controls.maxDistance = 20000;
    controls.panSpeed = 0.8;
    controls.keys = [ 'KeyA', 'KeyS', 'KeyD' ];
  } else {
    camera.position.set( 0, 0, 0 );
    controls = new FirstPersonControls( camera, renderer.domElement );
    controls.movementSpeed = 700;
    controls.lookSpeed = 0.05;
    controls.activeLook = true;
  }

  window.addEventListener( 'resize', onWindowResize );

}

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize( window.innerWidth, window.innerHeight );

  controls.handleResize();
}

container.addEventListener('click', onDocumentMouseClick, false);
container.addEventListener('mousemove', onDocumentMouseMove, false);
texteContainer.addEventListener('mousemove', (event) => {
  event.stopPropagation();
  document.body.style.cursor = '';
}, false);

let textesOpen = false;
function onDocumentMouseClick(event) {
  const mouse = new THREE.Vector2();
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = - (event.clientY / window.innerHeight) * 2 + 1;
  let raycaster = new THREE.Raycaster();
  raycaster.setFromCamera(mouse, camera);
  const intersects = raycaster.intersectObjects(listSolids, false);
  if(intersects.length > 0) {
    const texteChoisi = textes[intersects[0].object.name];
    if (textesOpen) {
      textes.forEach((el) => {
        el.classList.remove('show');
      });
      texteChoisi.classList.add('show');
      document.querySelector('.texte.show').scrollIntoView();
    } else {
      texteContainer.classList.add('show');
      texteChoisi.classList.add('show');
      if (!isTouchDevice()) controls.activeLook = false;
      textesOpen = !textesOpen;
    }
  }
}

const closeBtn = document.querySelector('.textes__close');
closeBtn.addEventListener('click', (e) => {
  e.preventDefault();
  document.querySelector('.texte.show').scrollIntoView();
  texteContainer.classList.remove('show');
  textes.forEach((el) => {
    el.classList.remove('show');
  });
  textesOpen = !textesOpen;
  if (!isTouchDevice()) controls.activeLook = true;
  closeBtn.blur();
});

function onDocumentMouseMove(event) {
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = - (event.clientY / window.innerHeight) * 2 + 1;
  let raycaster = new THREE.Raycaster();
  raycaster.setFromCamera(mouse, camera);
  const intersects = raycaster.intersectObjects(listSolids, false);
  if(intersects.length > 0) {
    document.body.style.cursor = 'pointer';
  } else {
    document.body.style.cursor = '';
  }
}

function animate() {
  requestAnimationFrame( animate );
  if (isTouchDevice()) controls.update();
  render();
}


function render() {

  const timer = 0.0001 * Date.now();

  for ( let i = 0; i < listSolids.length; i++ ) {
    const solid = listSolids[i];

    solid.position.x += 2 * Math.sin( timer + i * 1.1 );
    solid.position.y += 2 * Math.sin( timer + i * 1.1 );
    solid.position.z += 2 * Math.sin( timer + i * 1.1 );

    solid.rotation.x += 0.01;
    solid.rotation.y += 0.01;

  }

  if (!isTouchDevice()) controls.update( clock.getDelta() );
  renderer.render( scene, camera );

}

// Scripts

// Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);

// We listen to the resize event
window.addEventListener('resize', () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});

// Prevent bounce effect iOS
// Source https://gist.github.com/swannknani/eca799795860cff222f70b8675f8c8d8
// var contentBounce = document.querySelector('.container');
// contentBounce.addEventListener('touchstart', function (event) {
//   this.allowUp = this.scrollTop > 0;
//   this.allowDown = this.scrollTop < this.scrollHeight - this.clientHeight;
//   this.slideBeginY = event.pageY;
// });

// contentBounce.addEventListener('touchmove', function (event) {
//   var up = event.pageY > this.slideBeginY;
//   var down = event.pageY < this.slideBeginY;
//   this.slideBeginY = event.pageY;
//   if ((up && this.allowUp) || (down && this.allowDown)) {
//     event.stopPropagation();
//   } else {
//     event.preventDefault();
//   }
// });

//// Menu
const menuBtn = document.querySelector('.btn--menu');
const menu = document.querySelector('.menu');
const morceau = document.querySelector('.son');
const musiqueBtn = document.querySelector('.btn--sound');
const musiqueBtnOn = document.querySelector('.btn--sound .info--on');
const musiqueBtnOff = document.querySelector('.btn--sound .info--off');
let menuOpen = false;

menuBtn.addEventListener('click', (e) => {
  e.preventDefault();
  menuOpen = !menuOpen;
  if (menuOpen) {
    if (!isTouchDevice()) controls.activeLook = false;
  } else {
    if (!isTouchDevice()) controls.activeLook = true;
  }
  menu.classList.toggle('show--menu');
  menuBtn.blur();
});

// Musique
// var musiqueJoue = "pause";
let vol = 0;
let interval = 30;
let fadeInAudio;
let fadeOutAudio;
function musique(track) {
  if (track.paused) {
    clearInterval(fadeInAudio);
    clearInterval(fadeOutAudio);
    track.volume = vol;
    track.play();
    fadeInAudio = setInterval(function () {
    if (track.volume < 0.99) {
        track.volume += 0.01;
    }
    else {
      clearInterval(fadeInAudio);
    }
    }, interval);
  } else {
    track.pause();
  }
}
let silence = true;
musiqueBtn.addEventListener('click', (e) => {
  e.preventDefault();
  if (silence) {
    musique(morceau);
  } else {
    clearInterval(fadeInAudio);
    clearInterval(fadeOutAudio);
    morceau.pause();
  }
  musiqueBtnOff.classList.toggle('none');
  musiqueBtnOn.classList.toggle('none');
  silence = !silence;
  musiqueBtn.blur();
});

//
// Scrollbar with simplebar.js
//
let simpleBarContent = new SimpleBar(document.querySelector('.textes'));
// Disable scroll simplebar for print
window.addEventListener('beforeprint', (event) => {
  simpleBarContent.unMount();
});
window.addEventListener('afterprint', (event) => {
  simpleBarContent = new SimpleBar(document.querySelector('.textes'));
});

// Anciennes versions
let messageIOS = false;

const platform = window.navigator.platform;
const iosPlatforms = ['iPhone', 'iPad', 'iPod'];

if (!messageIOS && iosPlatforms.indexOf(platform) !== -1) {
  const banner = document.createElement('div');
  banner.innerHTML = `<div class="devicemotion--ios" style="z-index: 900; position: absolute; bottom: 0; left: 0; width: 100%; background-color:#000; color: #fff;"><p style="font-style: 1em; padding: 2em 1em; text-align: center;">Cet antilivre risque de ne pas fonctionner avec des versions d’iOS inférieures à la version 13.<br>(Cliquer sur cette bannière pour la faire disparaître).</p></div>`;
  banner.onclick = () => banner.remove(); // You NEED to bind the function into a onClick event. An artificial 'onClick' will NOT work.
  document.querySelector('body').appendChild(banner);
  messageIOS = true;
}
