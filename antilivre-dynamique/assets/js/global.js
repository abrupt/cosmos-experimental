//// Scripts

//// Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
//// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
//let vh = window.innerHeight * 0.01;
//// Then we set the value in the --vh custom property to the root of the document
//document.documentElement.style.setProperty('--vh', `${vh}px`);

//// We listen to the resize event
//window.addEventListener('resize', () => {
//  // We execute the same script as before
//  let vh = window.innerHeight * 0.01;
//  document.documentElement.style.setProperty('--vh', `${vh}px`);
//});

//// Prevent bounce effect iOS
//// Source https://gist.github.com/swannknani/eca799795860cff222f70b8675f8c8d8
//// var contentBounce = document.querySelector('.container');
//// contentBounce.addEventListener('touchstart', function (event) {
////   this.allowUp = this.scrollTop > 0;
////   this.allowDown = this.scrollTop < this.scrollHeight - this.clientHeight;
////   this.slideBeginY = event.pageY;
//// });

//// contentBounce.addEventListener('touchmove', function (event) {
////   var up = event.pageY > this.slideBeginY;
////   var down = event.pageY < this.slideBeginY;
////   this.slideBeginY = event.pageY;
////   if ((up && this.allowUp) || (down && this.allowDown)) {
////     event.stopPropagation();
////   } else {
////     event.preventDefault();
////   }
//// });

////// Variables & tools & functions
////let allVisible = true;
////let isGlitchedImage = false;
////const images = [...document.querySelectorAll('.image--crash')];
////const textes = [...document.querySelectorAll('.texte')];

////function randomNb(min, max) { // min and max included
////  return Math.floor(Math.random() * (max - min + 1) + min);
////}

////function shuffle(array) {
////  for (let i = array.length - 1; i > 0; i--) {
////      const j = Math.floor(Math.random() * (i + 1));
////      [array[i], array[j]] = [array[j], array[i]];
////  }
////}

////function shuffleImages() {
////  shuffle(images);
////  images[0].classList.add('show');
////}

////function shuffleTextes() {
////  if (allVisible) {
////    shuffle(textes);
////    textes[0].classList.add('show');
////  } else {
////    shuffle(textes);
////  }
////}

////shuffleImages();
////shuffleTextes();

////const textesAndImages = document.querySelectorAll('.images, .textes');
////for (const el of textesAndImages) {
////  // el.style.zIndex = randomNb(1, images.length);
////  Pace.on('done', function () {
////    animateDiv(el);
////  });
////}

////function animateDiv(el) {
////  let x = (Math.random() < 0.5 ? -1 : 1) * randomNb(20, 200);
////  let y = (Math.random() < 0.5 ? -1 : 1) * randomNb(20, 200);
////  let updates = 0;
////  anime({
////    targets: el,
////    translateX: x,
////    translateY: y,
////    duration: randomNb(2000, 4000),
////    easing: 'easeOutCubic',
////    complete: function (anim) {
////      el.setAttribute('data-x', anim.animations[0].currentValue.slice(0, -2));
////      el.setAttribute('data-y', anim.animations[1].currentValue.slice(0, -2));
////    },
////  });
////}



////const menuShuffle = document.querySelector('.btn--shuffle');
////menuShuffle.addEventListener('click', (e) => {
////  e.preventDefault();
////  // if (isGlitchedImage) {
////  //   let glitchedImg = document.querySelector('.image--glitch.show');
////  //   glitchedImg.remove();
////  //   isGlitchedImage = false;
////  // }
////  images.forEach((el) => {
////    el.classList.remove('show');
////  });
////  textes.forEach((el) => {
////    el.classList.remove('show');
////  });
////  document.querySelector('.image--glitch').classList.remove('show');
////  shuffleImages();
////  shuffleTextes();
////  menuShuffle.blur();
////});

////const menuMask = document.querySelector('.btn--mask');
////menuMask.addEventListener('click', (e) => {
////  e.preventDefault();
////  if (allVisible) {
////    allVisible = !allVisible;
////    textes.forEach((el) => {
////      el.classList.remove('show');
////    });
////  } else {
////    allVisible = !allVisible;
////    textes[0].classList.add('show');
////  }
////  menuMask.blur();
////});

////const menuGlitch = document.querySelector('.btn--glitch');
////menuGlitch.addEventListener('click', (e) => {
////  e.preventDefault();
////  document.querySelector('.images').classList.add('images--glitch');
////  let glitchParams = {
////    seed: randomNb(0,42),
////    quality: randomNb(70,99),
////    amount:  randomNb(0,99),
////    iterations: randomNb(0,42)
////  };
////  const imageActive = images[0];
////  let glitchedImg = document.querySelector('.image--glitch');

////  glitch(glitchParams)
////    .fromImage( imageActive )
////    .toDataURL()
////    .then( function( dataURL ) {
////      glitchedImg.src = dataURL;
////      imageActive.classList.remove('show');
////      glitchedImg.classList.add('show');
////    } );

////  isGlitchedImage = true;
////  menuGlitch.blur();
////});


////// Menu
//const menuBtn = document.querySelector('.btn--menu');
//const menu = document.querySelector('.menu');
//const morceau = document.querySelector('.son');
//const musiqueBtn = document.querySelector('.btn--sound');
//const musiqueBtnOn = document.querySelector('.btn--sound .info--on');
//const musiqueBtnOff = document.querySelector('.btn--sound .info--off');
//let menuOpen = false;

//menuBtn.addEventListener('click', (e) => {
//  e.preventDefault();
//  menuOpen = !menuOpen;
//  if (menuOpen) {
//    controls.activeLook = false;
//  } else {
//    controls.activeLook = true;
//  }
//  menu.classList.toggle('show--menu');
//  menuBtn.blur();
//});

//// Musique
//// var musiqueJoue = "pause";
//let vol = 0;
//let interval = 30;
//let fadeInAudio;
//let fadeOutAudio;
//function musique(track) {
//  if (track.paused) {
//    clearInterval(fadeInAudio);
//    clearInterval(fadeOutAudio);
//    track.volume = vol;
//    track.play();
//    fadeInAudio = setInterval(function () {
//    if (track.volume < 0.99) {
//        track.volume += 0.01;
//    }
//    else {
//      clearInterval(fadeInAudio);
//    }
//    }, interval);
//  } else {
//    track.pause();
//  }
//}
//let silence = true;
//musiqueBtn.addEventListener('click', (e) => {
//  e.preventDefault();
//  if (silence) {
//    musique(morceau);
//  } else {
//    clearInterval(fadeInAudio);
//    clearInterval(fadeOutAudio);
//    morceau.pause();
//  }
//  musiqueBtnOff.classList.toggle('none');
//  musiqueBtnOn.classList.toggle('none');
//  silence = !silence;
//  musiqueBtn.blur();
//});

//////
////// Interact.js
//////
////const angleScale = {
////  angle: 0,
////  scale: 1,
////};
////let zIndexDrag = 0;

////const targetInteract = '.images, .textes';
////interact(targetInteract)
////  .styleCursor(false)
////  .draggable({
////    listeners: {
////      start: function (event) {
////        event.target.style.zIndex = parseInt(new Date().getTime() / 10 ** 10) + zIndexDrag;
////        zIndexDrag += 1;
////        event.target.style.pointerEvents = 'none';
////      },
////      move: dragMoveListener,
////      end: function (event) {
////        event.target.style.pointerEvents = '';
////      },
////    },
////    inertia: true,
////    modifiers: [
////      interact.modifiers.restrictRect({
////        restriction: '.home',
////        endOnly: true,
////      }),
////    ],
////    autoScroll: false,
////  })

////function dragMoveListener(event) {
////  let target = event.target;
////  // keep the dragged position in the data-x/data-y attributes
////  let x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx;
////  let y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

////  // translate the element
////  target.style.webkitTransform = target.style.transform = 'translateX(' + x + 'px) translateY(' + y + 'px)';

////  // update the posiion attributes
////  target.setAttribute('data-x', x);
////  target.setAttribute('data-y', y);
////}

////// this function is used later in the resizing and gesture demos
////window.dragMoveListener = dragMoveListener;

////
//// Scrollbar with simplebar.js
////
//let simpleBarContent = new SimpleBar(document.querySelector('.textes'));
//// Disable scroll simplebar for print
//window.addEventListener('beforeprint', (event) => {
//  simpleBarContent.unMount();
//});
//window.addEventListener('afterprint', (event) => {
//  simpleBarContent = new SimpleBar(document.querySelector('.textes'));
//});
