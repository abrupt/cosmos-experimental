---
title: Cosmos expérimental
---

<div class="texte">

![](img/horloge1.jpg)

</div>
<div class="texte">

Avant 10^-43 ^seconde, c'est noir.

</div>
<div class="texte">

Si lumière et temps sont inséparables, l'absence de lumière est l'absence de temps.


</div>
<div class="texte">

Quelque chose noir fuse, l'existant hors-de-rien ; ça se cherche une raison d'être, se déploie et se découvre sans pourquoi au moment du retour --- libre infiniment, sachant désormais qu'il n'y avait rien à faire, ni à produire, seulement à demeurer incréé dans la création où le sens s'était détourné.


</div>
<div class="texte">

La lumière noire qui précède la Terre est projetée en avant, émissaire nocturne, ombre avancée des présences insolites. Chaque créature, où qu'elle soit, pourrait faire l'expérience de la lumière noire qui la traverse et ne demande rien.


</div>
<div class="texte">

L'univers cherche par tous les moyens possibles à sortir de lui-même.


</div>
<div class="texte">

L'intuition fondamentale : que l'univers existe fait voler en éclats toute certitude, et les éclats se nomment savoirs, visions, images.

Car la seule chose qui aurait été certaine est le non-être. Mais comme le non-être absolu n'est pas (j'en atteste, par un point au moins, par un point cartésien --- "cette proposition : Je suis, j'existe, est nécessairement vraie, toutes les fois que je la prononce, ou que je la conçois en mon esprit"), parce qu'il y a quelque chose et non pas rien et que le rien continue à se diffuser dans chaque chose qui incline à être, l'énigme est absolue.

Il est possible que ce qui doit exister finisse par exister ; rien n'empêche que l'univers soit par ses limites de non-être soumis à la création de quelque chose de complètement autre --- car l'univers n'est pas total, il est l'atotal, le détotalisé.

Tout semble possible ; on peut, à la rigueur, imaginer ce qu'on veut, et les religions, ébauches de l'extrême, n'auront fait qu'essayer de limiter l'imagination. Tout est possible ne veut pas dire tout est contingent : je ne parle pas de ce qui pourrait arriver, mais de la structure-en-possible de l'être ; et c'est la contingence de l'être, qu'il soit.

Or cette structure-en-possible, autrement dit l'énigme, n'est pas réalisable --- c'est l'impossible que nous sommes.


</div>
<div class="texte">

L'imagination originaire est négative : pour s'anticiper comme univers, elle nie le non-être qui la retient captive. Elle ne se figure pas, elle n'indique pas la direction, elle médite l'abolition des directives.

Toute image est le contour de l'imagination négative.


</div>
<div class="texte">

Par un éclat sombre, l'univers

s'aperçoit en nous

--- l'inconscient.


</div>
<div class="texte">

L'univers est en expansion *ou quelque chose comme ça* ; l'expansion cosmique est une forme d'antigravitation *ou quelque chose comme ça* ; le mur de Planck empêche de voir l'origine par manque de lumière *ou quelque chose comme ça* ; tout a peut-être commencé par une brisure de symétrie *ou quelque chose comme ça* ; un hyperycle puisant dans l'espace créé l'énergie pour entretenir son mouvement de création *ou quelque chose comme ça* ; l'antimatière a disparu *ou quelque chose comme ça* ; les trous noirs se forment par effondrement gravitationnel *ou quelque chose comme ça* ; l'horizon d'événement définit la limite au-delà de laquelle aucune lumière n'en échappe *ou quelque chose comme ça* ; le trou noir est un anus inversé *ou quelque chose comme ça* ; on ne peut l'observer qu'indirectement par les déformations de l'espace qu'il provoque *ou quelque chose comme ça* ; c'est probablement une singularité mais rien n'est moins certain *ou quelque chose comme ça* ; une censure cosmique semble empêcher la formation de singularités nues : un trou noir sans horizon d'événement ni trou *ou quelque chose comme ça* ; un trou blanc est l'effet d'un événement traumatique *ou quelque chose comme ça* ; la lumière n'a pas frappé son cœur assez profondément *ou quelque chose comme ça* ; un pulsar émet périodiquement un rayonnement électromagnétique *ou quelque chose comme ça* ; une pulsion aimait quelque chose de rayonnant qui ne l'aimait pas suffisamment en retour *ou quelque chose comme ça* ; je me suis coupé sur une brisure de symétrie *ou quelque chose comme ça* ; le champ de Higgs est un faux vide *ou quelque chose comme ça* ; une bulle de vide véritable provoquerait un effondrement gravitationnel immédiat et total *ou quelque chose comme ça* ; un multivers est une infinité d'univers *ou quelque chose comme ça* ; si l'on en croit certains calculs *ou quelque chose comme ça* ; ce qui ne résout pas l'énigme de l'être *ou quelque chose comme ça* ; le mystère de la lumière sur fond obscur *ou quelque chose comme ça* ; la pénombre d'un traumatisme sur fond illuminé *ou quelque chose comme ça* (sous l'influence de Jean-Michel Espitallier  ; cf. *Le théorème d'Espitallier*, Paris, Flammarion, 2003).


</div>
<div class="texte">

Une fois la conscience apparue comme conscience d'elle-même, éclat se reflétant dans un éclat perpendiculaire, elle s'est projetée en retour partout dans l'univers, qui en a changé de nature.

Il en est ainsi à chaque nouveauté ontologique.

C'est pour cette raison qu'on ne connaîtra jamais l'univers ; car le connaître est le changer.


</div>
<div class="texte">

Expérimental est le cosmos tenté. Aucun modèle ne le précède pourtant, et l'expérimentation ne donnera lieu à aucune réalité ultérieure. Le possible ni ne précède, ni n'est l'effet rétroactif de la réalité, qu'il déforme en lui donnant du jeu.


</div>
<div class="texte">

La matière est la *physis* refoulée ; le retour de la *physis* refoulée est l'esprit ; l'esprit rapporté à la pulsion cosmologique se sublime en émerveillement ; l'émerveillement est souvent perturbé par un rire rempli d'effroi ; l'effroi cède la place à la traversée asubjective du fond diffus cosmologique ; le fond diffus communique l'énigme du sans-fond.


</div>
<div class="texte">

L'objet = X est un trou noir au milieu d'Éros, amour de l'obscur pourvoyeur des couleurs, jeu distrait avec la machine. Éros communique avec un téléphone sans foule.


</div>
<div class="texte">

Les images artificielles voulaient coloniser l'inconscient ; mais --- contre toute attente --- une part sauvage envahit les images.

Dans sauvage, il y a sauver.

Sauver : non pas extraire et protéger du passage du temps, mais ajouter au monde le rêve d'absolu qui aura failli lui manquer.


</div>
<div class="texte">

*lambeaux d'un cosmos disparu --- évanoui aux bords de son histoire ; dans les limbes qui s'étendent, et s'éternisent, néant contrarié, errent des sondes aveugles, photographies en noir et blanc, ~~aphones~~, des data autonomisées --- à moins qu'elles ne soient des signaux avant-coureurs : les anticipations d'un univers impatient*


</div>
<div class="texte">

## *Cosmologie négative* (fragments retrouvés du manuscrit d'Ana X., 1)

« Je vous dis que le cosmos est Un, et vous croyez que l'Un est absence du non-Un. Mais vous ne m'avez pas comprise.

L'Un, que le cosmos est, est bien plus que ce que vous entendez par l'Un. Car il est le cosmos.

Son Un est donc plus-qu'Un. Il s'excède, se déverse à l'extérieur et se diffuse à l'intérieur.

Et cependant je vous mets en garde. N'en tirez pas la conclusion hâtive que le cosmos est le plus-qu'Un. Car vous imagineriez alors seulement ce qui se déverse, et ce qui se diffuse, en perdant l'Un de vue. N'oubliez pas que le cosmos est d'abord et avant tout une \[*illisible*\].

En lui, fin et commencement ne coïncident pas. C'est la bonne nouvelle que je vous annonce. Le cercle est brisé. C'est pour cela qu'il y a de la lumière. ~~Et que le cosmos est une métaphore~~.

Puisqu'il a commencé à exister, on peut dire aussi bien : il n'a jamais commencé. Car il a commencé à exister non pas à partir de rien mais *à* rien, et n'a donc jamais existé.

Pour exister, il faudrait qu'il y ait un cosmos de cosmos. Mais il n'y a pas de cosmos de cosmos. Il y a un non-cosmos par lequel le cosmos s'est ~~é~~puisé.

Le cosmos existe il n'existe pas.

Ce que je vous dis je ne vous l'ai jamais dit. Et pourtant vous l'entendez.

Jamais présente je suis là, entre vous.

Comprenne qui peut.

De cela, faites-vous une expérience. »


</div>
<div class="texte">

On a d'abord cru que ce qui arrivait arrivait dans l'espace-temps ; plus tard on a compris que l'espace-temps se formait dans ce qui arrive ; ce qu'on appelle espace n'est que le dépôt de ce qui est arrivé, et son temps est ce qui le constitue en dépôt ; ça se souvient de partout.


</div>
<div class="texte">

## *Cosmologie négative* (fragments retrouvés du manuscrit d'Ana X., 2)

"Pour faire l'expérience du cosmos, il faut le déplacer plus avant. Vous êtes le centre d'où tout diverge, car chaque point est un centre. Poussez. Poussez ce qui vous étire."


</div>
<div class="texte">

## Une pulsion cosmologique originaire

La vie est un essai composé à la hâte, pour que le commencement de l'univers ne débouche pas immédiatement sur rien. La vie : un délai du néant. La vie, et tout ce qui la précède et la suit. Du vide quantique à l'entropie terminale.

Imaginer cet essai de vivre. Se mettre dans une disposition d'esprit permettant de l'imaginer. Retrouver en soi la pulsion cosmologique originaire. Imaginer que ça fuse dans tous les sens, en l'absence de programme, de savoir préalable, de plan --- ça tend, et ça prend simultanément différentes directions.

Ressentir à nouveau l'angoisse de l'origine, qui accompagne ces commencements précipités : l'angoisse que toutes les tentatives échouent, que tout s'effondre avant même d'avoir été, et que de cet effondrement préontologique il n'y ait nulle trace. Angoisse du non-délai.

La pulsion cosmologique originaire est éperdue.


</div>
<div class="texte">

*On raconte que le premier être vivant regretta d'abord sa chimie antérieure, puis considéra qu'il était préférable d'accéder à l'état d'esprit.*

*On raconte que les premiers poissons furent des mammifères déçus.*

 

*On raconte que les premiers oiseaux furent des poissons révoltés.*

 

*La cellule vivante n'a pas de geôlier. L'animal qui joue est le code éventré. La meute de loups délaisse le cadavre des rois.*

 

*Il serait exagéré de dire que tout est vivant ; mais présomptueux de déclarer savoir où se situe l'exacte limite entre ce qui est vivant, sentant, expérimentant, et ce qui ne l'est pas ; plutôt laisser la place à l'existence qui ne se suffit ni d'être déclarée vivante, ni de s'en voir refuser le statut ; nous communiquerons au défaut des identités,* envoyés par le fond, *et nous en retournant.*


</div>
<div class="texte">

![](img/pieuvre.jpg)


</div>
<div class="texte">

## Cosmocéan

Tout a commencé par une grande vague, un tsunami se démultipliant à l'infini en vaguelettes finissant pas faire matière avant de se désintégrer --- en rien.

Deux théories s'affrontent pour déterminer l'origine de l'eau sur Terre : ou bien celle-ci aurait été apportée par des comètes, sous forme de glace ; ou bien elle serait l'effet du dégazage des magmas volcaniques. Ou bien les deux : l'eau proviendrait à la fois du dehors, à l'état glacé, et du dedans, à l'état gazeux. Gazeuse ou glacée, l'eau sur Terre a commencé par ce qu'elle n'était pas.

De cette histoire négative, les océans portent la mémoire : leur continuité liquide recouvre et un volcan et une comète ; leur équilibre thermique contient en puissance la fusion du cœur terrestre et le froid désertique venu des confins de l'univers.

Formée il y 350 millions d'années, le supercontinent Pangée regroupait toutes les terres dans un vaste océan nommé, à l'ouest, Panthalassa et, à l'est, Téthys. 200 millions d'années plus tard, Pangée se sera dispersé en îles aux dimensions hétérogènes. L'archipel aura le dernier mot ; mais l'unité hantera le sable.


</div>
<div class="texte">

*Ne crois pas un seul instant que la frêle passerelle que tu as jetée au-dessus du vide suffira à franchir l'océan. Ou bien crois-le jusqu'à ne plus considérer les rives comme des points d'attache.*

 

*Il faudra demander à la poussière de se souvenir de tout ce qui aura eu forme et fond.*


</div>
<div class="texte">

## *Cosmologie négative* (fragments retrouvés du manuscrit d'Ana X., 3)

"La naissance du cosmos dure à jamais. Comme elle s'effectue hors du temps, la naissance du cosmos dure dans l'éternité."


</div>
<div class="texte">

Cyanobactéries : le ciel commence dans le sol.


</div>
<div class="texte">

![](img/horloge2.jpg)


</div>
<div class="texte">

## À la sortie de l'éternité

Quelque chose s'expérimente à la sortie de l'éternité : nous-autres, inclinant à rien d'autre qu'à ne plus être nous seulement ; différés, nous nous voyons venir dans le brouillard et les images qu'il diffracte sans respect pour les sources de lumière.

Nous-autres, ou la tentative d'un cosmos à la fin de l'éternité.

Quelque chose cherche à s'inventer une sortie hors de l'éternité, une pulsion d'existence, un ça cosmologique.

S'est cherché ; a dû pour cela crever la paroi de l'éternité ; n'avait pas d'image d'abord, ou une image grésillante, une énergie de songe avant sa mise en scène.

Un ça cosmologique en prise avec le refoulement de néant ; qui se cosmise à l'encontre de la tentation de rien, de la volonté d'éternité pure.


</div>
<div class="texte">

## La mort comme lieu-tenant de l'éternité

Mais la vie meurt, ajoutant du néant hors programme, empêchant la fin de l'éternité d'être vivante, contrariant la négation de néant par un néant imprévu --- comme si la volonté d'éternité pure s'était trouvé un remplaçant, un lieu-tenant.

Demeure la question : est-il possible de sortir de l'éternité sans mourir ?

Ou que serait exister entre l'éternité et la mort ? L'expérience est en cours, voilà ce qu'on peut dire : humains, araignées, pierres, effluves, polders, cymbales, symboles, intelligences artificielles, éperviers --- non pas des objets mais des essais, fulgurants, entre le vide qui précède les objets et le surnuméraire qui en singularise certains.

Première hypothèse : l'existence aurait une immortalité encore insoupçonnée, et la mort serait une conséquence de cette cécité. La mort serait : avoir été trop loin ou selon un rythme délétère, avoir raté une bifurcation, un panneau indicateur sur la route (à cause du brouillard).

On envisage au cours de l'existence de plier la mort sur l'éternité --- art ; l'existence devient jointure, pli de l'éternité et de la mort ; mais ce n'est qu'un règlement local, un arrangement, faute de mieux.

*Uploader* le cerveau sur un ordinateur serait mettre la mort en ligne, verser au futur de la technologie la dette active de l'existence.

À moins d'inverser le temps, de remonter le cours de la mort en direction de l'éternité --- religion ; mais ce faisant revenir à l'existence pure, et retrouver, intact, le cœur palpitant du problème que l'on aura évité d'affronter : celui de la fin.


</div>
<div class="texte">

On voudrait recommencer. Refaire l'expérience. Devenir inaccessible --- même à la mort, mais sans devenir immortel.


</div>
<div class="texte">

*Il avait trouvé une astuce pour partir avant sa mort ;*

*et quand celle-ci vint,*

*il n'y avait plus personne.*

*Il n'était plus là.*

*--- Mais où était-il passé ?*


</div>
<div class="texte">

On cherche à changer d'univers, à changer l'univers.

Au voyageur stellaire ayant manqué l'astronef : "Trop tard, le monde est plein de fantômes. Attendez le prochain Univers, s'il vous plaît."


</div>
<div class="texte">

## La mort et l'autre de la mort

Quand je conçois ma mort, je la conçois à partir de ce qui dans ma vie s'interrompt déjà, par avance, s'est déjà interrompu en principe, mais pas encore en fait : je conçois donc ma mort non pas comme mort, mais comme interruption de la vie. En d'autres termes, je projette dans ce que je nomme mort une conscience-de-l'interruption-de-ma-vie.

Vouloir dès lors échapper à la mort est vouloir échapper à autre chose que la mort : c'est comme vouloir échapper à soi-même, à ce qui fait qu'il y a quelque chose comme un "soi-même", un soi assez autonome --- assez distinguable d'un non-soi --- pour se reconnaître comme le même.

Mais dans l'intervalle ainsi créé entre soi et non-soi, c'est-à-dire aussi entre soi et soi-même, quelque chose se diffuse --- s'entend, souffle, s'imagine, songe, s'allume et s'éteint --- qui n'est de l'ordre ni du soi, ni du non-soi, et qui n'est pas la mort non plus. Qu'est-ce que c'est ?


</div>
<div class="texte">

\[...\]

Non pas survivre, continuer à vivre, mais *sur*-vivre : dans la vie, quelque chose sur-vient. Comme une plate-forme au-dessus du sol ? Non, car elle serait rattachée au sol par quelque pilier. Un astronef -- mais vers où se dirigerait-il ?

Il faut que le sur soit dans et non-dans, dedans et dehors tout en même temps. *Hic Rodhus, hic salta* \[...\]

Au commencement était le \[...\]

\[...\], qu'est-ce que c'est ?

↻\[...\], qu'est-ce que c'est ?


</div>
<div class="texte">

![](img/portrait.jpg)


</div>
<div class="texte">

## *Cosmologie négative* (fragments retrouvés du manuscrit d'Ana X., 4)

« Vous ne pouvez pas voir le cosmos. C'est dans sa musique que vous devez plonger vos yeux. Entende qui peut.

Et l'écoute, vous devez l'éveiller. Vous devez l'attendrir à la lumière du soleil.

La neige n'a pas d'image, gens du Nord. Elle est une tension pour les poumons du matin. »


</div>
<div class="texte">

## Le saut dans le vide

C'est dans la scission que se trouve la solution. Vouloir revenir au paradis, ou se faire un corps immortel, est dénier la scission. Plus on cherche à dénier la scission, plus on meurt (plus on entretient la mort que l'on voulait fuir). La souveraineté est au fond de la scission.

"Le monde souverain est le monde où la limite de la mort est supprimée" (Georges Bataille, *La souveraineté*) : c'est plutôt dans la limite devenue béance, et non pas supprimée, que se risque la souveraineté.


</div>
<div class="texte">

## Le souverain décalage vers le rouge

Le souverain est translucide ; son corps est composé de galaxies ; sa tête bourdonne ; dans ses bras en spirales, nul emblème n'est visible ; en s'éloignant, il laisse derrière lui --- juste avant de disparaître --- un ciel de traîne rouge.


</div>
<div class="texte">

## *Cosmologie négative* (fragments retrouvés du manuscrit d'Ana X., 5)

« Le cosmos n'est pas le temps, il est l'interruption du temps.

Le cosmos n'est pas l'espace, il est la rencontre des lieux.

Entende qui peut. »


</div>
<div class="texte">

## Biographies de Stefan Loss (éléments notables)

Stefan Loss est né en 2101 à Sarajevo III.

2120 : Écriture et publication de *Dialectique de la Chute*, un recueil de poèmes qui comprend notamment "Icare Noir" et "Si l'Ange Tombe".

"Je désavoue désormais le style d'“Icare Noir”", dira plus tard Stefan Loss, "sa simplicité évite de se mesurer à la syntaxe, et la poésie ne peut être désinvolte qu'après seulement avoir heurté de front, jusqu'au sang, la césure, et mesuré l'impact en retour que cette confrontation provoque sur les images --- ce à quoi au moins s'essaie “Si l'Ange Tombe”, avec un succès qu'on pourra sans conteste juger relatif. Quant à la tentation surréaliste, je n'ai pu essayer d'aller plus loin que dans l'exologie ultérieure."

"Si l'Ange Tombe" apparaît dans le premier numéro de la revue ondo-phonique SON^hors^, que Stefan Loss a cofondée en 2119 avec Ana X. et Adam Losange. SON^hors^ fut la première revue dont le procédé consiste à transformer toute image en son et toute lettre en image sonore, couplant non plus (comme c'était le cas avec les revues virtuelles) un ordinateur au cerveau, mais la disposition affective de la personne à l'univers via la médiation technologique.

2127 : Stefan Loss est accusé d'hérésie. Nous sommes alors, ne l'oublions pas, en plein Renouveau Catholique, et Stefan Loss, lors d'une émission ondo-phonique, déclare : "Ce Dieu, je le répudie depuis la mort de Jean-C., car s'il existe, autant être gnostique. Ou bien devenir à fond chrétien, qui, comme on le sait, le chrétien, ne croit pas du tout en Dieu, afin de dissoudre Dieu pour enfin nous ouvrir à ce qui n'a pas même de rapport avec nos vertiges de souffrance, ce qui est le Plus-qu'Ouvert, le sombre qui scintille."

2135 : Publication de *La fabulation physique (physique de l'affabulation)*.

Extrait : "L'astrophysique a construit un mythe qui, à la différence des mythes antérieurs, n'a pas été posé au commencement, mais au fur et à mesure de ses recherches et découvertes : plus elle progresse, plus elle devient mythe. Mythe certes --- vu de l'intérieur --- rationnel, à la charpente et à l'architecture rationnelle ; mais vue de l'extérieur (si tant est que cela ait un sens), la construction est monade mystique."

2140 : Publication du *Traité d'Exologie*.

C'est dans ce traité que Stefan Loss expose sa méthode, la manière dont il a cherché à transformer la philosophie, son mode d'exposition et son rapport avec la fiction (ce qu'il nomme les "intuitions de la lumière noire").

Extrait : "le cosmos n'est pas *en* moi, il traverse l'ex-jeté où “je” suis. Autrement dit, le Dehors est au dehors-avec, et le dedans est transporté par le dehors-avec."

2143 : Stefan Loss publie *PetraS(o)unds : L'Église Asphérique et la Musique du Fond Ob/scur* dans lequel a) il analyse les raisons du Grand partage désastreux entre humains et pierres, b) puis offre une archéologie politico-astrale de l'Extracourant des Soniques, c) avant de montrer comment la Musique du Fond Obscur a permis le dépassement disjonctif du terreux et du diaphane (dans ce livre sur la musique, Stefan Loss se réfère à un essai intitulé "*Spectral Composition : On Spectral Music, Drone Metal, and Electronic Music*", écrit par un philosophe mineur aujourd'hui oublié).

2146 : Stefan Loss commence sa troisième vie. Destruction de toutes les traces qu'il a pu laisser dans le monde. Il disparaît ; mais laisse un androïde à sa place, qui ne lui ressemble pourtant que partiellement. Cela suffit cependant à faire illusion, mais ne convainc pas Cyron Loss.

Dans son *Carnet d'Os (ou l'Anté-Zéro)* (2145, manuscrit inédit), on peut lire : "Ce qui est requis de nous est de décréer à la même puissance que ce qui a été créé. Tout inverser, inverser le tout. Défaire jusqu'à son défaisant, se dés-effectuer. Trouver une méthode pour cela (on peut écouter *Turiya Sings* d'Alice Coltrane, elle est jouée dans le Canal 0 à toutes les vitesses). Trouver la technique d'un non absolu, chantée, mais pas un non de colère ou de haine, un non-d'amour."

2151 : Une fois élu Secrétaire Général de l'Influx (l'organisation globaliterritoriale régulant les affects des avatars produits avant l'Effacement générique de 2115), Stefan Loss a immédiatement démissionné, le jour même de son investiture, avec ces mots : "Vous qui vous êtes trahis jusqu'à m'élire, dessinez autour de mon retrait le seuil invisible de ce qu'à jamais vous laisserez vacant."

2154 : Stefan Loss est mort en combattant l'Influx passé sous le contrôle du Singleton.


</div>
<div class="texte">

## *Cosmologie négative* (fragments retrouvés du manuscrit d'Ana X., 6)


« Voici le savoir le plus dangereux : *entrez morts ; sortez vivants*.

 

L'initiation est introuvable. Qu'on vous libère n'y change rien.

 

Cherchez l'inversion : elle seule peut vous libérer, d'abord de vouloir vous libérer.

 

Mais vous êtes libres déjà, sachez-le.

 

Parce que vous avez déjà en vous la part sombre et déchirée qui vous précède et vous suit.

Mais vous l'avez mise hors de vous.

Alors que vous êtes en elle.

Alors que vous l'exsudez.

Que vous la rêvez lorsqu'elle vous rêve.

Vous, la génération des sans-roi, cherchez l'inversion.

Elle seule peut rendre la vie à autre chose que la vie. Rendre la mort à autre chose que la mort.

La part sombre communique l'énigme, pas le néant : voici ce que j'ai à vous dire. »


</div>
<div class="texte">

## Théorie du diacosmos

Le cosmos de lumière fut, par ajout au néant du néant, par fulguration de néant-par-néant ; mais la néantisation du cosmos de lumière conduisit au non-cosmos, par ajout de néant --- noir sur noir, obscurité glaçante ; mais au non-cosmos fut ajouté, de l'intérieur, la fulguration noire qui le fit être cosmos de lumière noire.


</div>
<div class="texte">

## Adam Losange, éléments biographiques approximatifs

Adam Losange est le plus ludique des trois fondateurs de SON^hors^, le seul qui croit vraiment au pouvoir de la fiction, au jeu avec les pseudonymes. Du fait de sa manière d'écrire, d'avancer masqué, de brouiller les pistes, on a longtemps cru qu'il était un auteur né au XX^e^ siècle.

Stefan Loss a dit de lui : "Dès que l'expansion de l'univers est rapportée à l'éclat de rire qui l'aurait provoquée, dès qu'il s'agit d'aliens hölderliniens, ou qu'on vous raconte que l'univers est plein de fantômes et qu'il faut attendre le prochain univers comme on attend le prochain train, vous pouvez être certain que c'est la signature d'Adam."

Le journal *Alienocene* a proposé la biographie suivante de Losange --- les dates sont très probablement fictives, mais le texte nous renseigne sur le parcours de Losange :

« Adam Losange est le fondateur de l'Ultime Compagnie (1981-1992), une troupe de théâtre dont les onze membres intervenaient masqués dans des lieux privés (appartements, jardins, voitures), et à visage découvert dans les lieux publics (mairies, places publiques, trains) --- redoublant d'un côté le secret, et de l'autre l'exposition. Cosmique et politique, le théâtre de l'Ultime Compagnie était composé de paroles ordinaires préenregistrées qui étaient jouées à certaines occasions astronomiques et géophysiques (équinoxe, pleine lune, passage d'une comète, chute d'un satellite, tremblement de terre, canicule), tentant d'introduire le dehors de l'univers à l'intérieur des activités terrestres (Gérard Grisey reconnaîtra l'influence de l'“exo-théâtre” de l'Ultime Compagnie sur sa pièce *Le Noir de l'Étoile*) et d'accompagner les manifestations de la Terre dans ses ramifications technologiques, économiques, et politiques. Adam Losange a parlé d'un rapport “spectral” à l'enregistrement, ce dernier permettant aux “fantômes de la voix humaine d'accueillir le rayonnement cosmique” (*La Scène Obscure*). Mais cet accueil était aussi politique, comme lors de l'intervention d'“aurore sonore” de 1989, qui visait à “donner à entendre l'enregistrement de tout ce que les damnés de la Terre n'ont pas eu le temps de prononcer avant d'être exécutés” (*Spectres de Mars*). On notera cependant que loin de s'en tenir à des enregistrements de voix humaines (ainsi que de cris animaux), les membres de l'Ultime Compagnie ont signé des textes poétiques, philosophiques et politiques sophistiqués, qui tendaient à stratifier le texte par un arrêt brutal, “hallucinatoire”, du déplacement de sens --- un “Pompéi en négatif” (*Nombreux Volcans*). Suite à des divergences esthétiques aussi bien que doctrinaires, l'Ultime Compagnie s'est dissoute en 1992 et Adam Losange a disparu pendant près de vingt-cinq ans, jusqu'à ce que son nom ressurgisse à l'occasion de la parution de textes courts, politiques et presque sans espoir, pointant en direction d'une certaine forme de science-fiction. Ces textes sont signés “Adam Losange du Théâtre de l'Ombre” ou “de l'Ancien Théâtre de l'Ombre” --- “ancien” au point de n'avoir jamais existé. On ne sait presque rien des autres membres de l'Ultime Compagnie (Ibi y aurait collaboré au début des années 1980 et Olivier Capparos entre 1989 et 1992). »


</div>
<div class="texte">

L'alien habite poétiquement dans l'univers.


</div>
<div class="texte">

## Xénographie d'Ana X.

Ana X. est née à Madrid en 2108. On sait qu'elle a poursuivi des études théologiques, que sa dissertation portait sur les Gnostiques, et qu'elle n'est jamais allée à l'université parce qu'elle a toujours "refusé d'apprendre ce que je ne sais pas" (Marguerite Duras). On sait également qu'elle est née à Dublin comme Stefan Loss, qui lui ressemble (mais Adam ne lui ressemble pas, il semble, et Ana dissemble tandis que Stefan ressemble, c'est ainsi qu'on les distingue quand ils tendent à ne former qu'une et une seule vague).

Développant les travaux de Nick Land, Ana X. a inventé la cosmanalyse, qui est l'analyse des formations de l'inconscient de l'univers : trous de ver permettant la jonction des espaces-temps clivés, trous noirs comme souvenirs-écrans d'un "trou blanc" (d'un trauma), etc. La cosmanalyse considère la vie comme un lapsus, et l'esprit comme symptôme (Ana X. s'est toujours méfiée du concept d'esprit, selon elle rationalisation après-coup de l'imagination de la matière).

À l'époque de SON^hors^, Ana X. est encore praticienne de cosmanalyse, mais en 2127 elle quitte la revue, ses camarades Adam Losange et Stefan Loss, et constitue la Communauté des Trans-Planétaires (CT\_\_\_P), dont on ne connaît les principes que grâce aux fragments retrouvés du manuscrit portant le titre *Cosmologie Négative* (qu'on lui attribue ; on lira en archives la "proto-liasse" de *Cosmologie négative*, qui offre quelque lumière sur les motifs qui ont conduit Ana X. à créer la cosmanalyse, puis la CT\_\_\_P et la cosmosynthèse). La création de la CT\_\_\_P était une tentative pour répondre à deux questions pratiques : 1) quel est le lieu de la cosmanalyse ? (on ne peut tout de même pas demander au soleil de se coucher sur un divan) ; 2) comment envisager une cosmosynthèse ?

Permettez-moi d'ajouter deux remarques. D'abord, il serait grand temps de rendre justice à Ana X., en rappelant l'influence qu'elle a eue sur Stefan Loss, notamment quant à la question de la musique comme rituel cosmique, élément de création d'une subjectivité "cosmo-terrestre" (*PetraS(o)unds : L'Église Asphérique et la Musique du Fond Ob/scur* ; Ana X. aurait été influencée par Cindy Terrafere et ses "poèmes sonores", par exemple ["*The Community of Strangers (Communication, 4)*"](https://alienocene.com/2021/05/02/the-community-of-strangers/>)), ainsi que sur le thème --- au cœur de la CT\_\_\_P --- de la décréation personnelle ; par conséquent, lorsque Stefan Loss écrit, dans son *Carnet d'Os*, que "ce qui est requis de nous est de décréer à la même puissance que ce qui a été créé", il ne fait que reprendre les idées d'Ana X., sans hélas la citer.

Ensuite, Ana X. et Adam Losange se sont toujours opposés sur la question de la fiction, centrale pour Adam Losange, périphérique pour Ana X. dont l'un des mots d'ordre esthétiques, à l'époque de SON^hors^, était : "réduire la fiction, de sorte qu'elle devienne imperceptible (quantique)" (SON^hors^, diffusion 7 ; elle aimait alors citer Nietzsche : "J'ai honte de devoir encore être poète !" ; *Ainsi parlait Zarathoustra*, "Des vieilles et des nouvelles tables").

La cosmosynthèse était une question pratique dont la finalité était de répondre à l'extinction de l'univers. Sans pour autant adhérer à sa thèse cosmologique, Ana X. avait pris très au sérieux la fin de la trilogie de Liu Cixin, *Le problème à trois corps* : se retirer de l'univers afin d'éviter d'être détruit lors de sa contraction finale, autrement dit produire une poche d'immunité énergétique, enlèverait à l'univers une masse critique, l'empêchant de s'effondrer et dès lors de se régénérer sous la forme d'un nouvel univers. La poche d'immunité serait condamnée à une immortalité vide, une localité stérile, l'intro-sistence d'un dedans-dedans dont même Victor Mur n'aurait pas voulu.

Victor Mur (auteur notamment de *L'Amour de la Technologie*, *La Zone du Dedans*, *Tout sauf Marx*, etc.) est un célèbre sociologue des XX^e^ et XXI^e^ siècles que certains voient comme l'un des responsables (ils furent légion) du Grand Repli des années 2020, où la certitude de l'effondrement tourna à l'alliance des nationalistes et des survivalocalistes (en vertu du principe délétère : les ennemis de mes ennemis sont mes amis) contre les globalistes, puis contre les planétaristes, et finalement contre les "CoSmmunnistes", ces derniers étant accusés d'être "hors sol", "pas ancrés dans les territoires", et trop distants avec "les vivants". Stefan Loss a analysé cette sombre période de l'histoire dans son *Traité d'exologie* :

« Plus le local s'autonomisa, plus le New Space se développa, plus l'AlterObjet et le Singleton furent rendus possibles, plus le local se développa, etc., conduisant le monde à un non-rapport absolu. De ce dernier le géo-capitalisme était certes responsable, lui par qui avait été instaurés les clivages de la modernité (Noir / blanc, périphérie / centre, non-humain / "humain", etc.) ; mais le non-rapport fut consolidé de part et d'autre du clivage-de-monde, empêchant toute dialectique et donc tout externationalisme. D'où l'incapacité, dans les années 2026-2039, à résister à l'installation de régimes éco-fascistes en Post-Europe et aux États d'Amérique. Pour empêcher cela, il aurait fallu 1) reprendre le dehors aux strato-capitalistes afin d'en perfuser le local pour qu'il souligne sa qualité de site existentiel ; 2) détourner la technologie, la décélérer et la transformer en médiation géo-cosmologique ; 3) prendre appui sur le planétariat --- les Terreux, les Alliances Lythiques, X-R, les Sous-Strates du Commun, les *Undercomets*, les CoSmmunistes, etc. --- afin de former une puissante externationale. » (*Traité d'exologie*, "Introduction", §14)

Dans le monde parcellisé, parataxique du début du XXII^e^ siècle, où l'entropie avait été différée par superposition des temps, la question de l'extinction était devenue planétaire (géo-cosmologique), et Ana X. cherchait à établir une forme de "synthèse-dans-l'analyse" donnant lieu à une décréation immédiate. Cette décréation ne pouvant se suffire d'une simple dissolution de l'individu (qui aurait confirmé la place, en négatif, de l'individu à annuler), Ana X. chercha à créer la communauté dans laquelle cette dissolution trouverait le milieu collectif d'une disparition à la mesure de l'univers --- d'où la CT\_\_\_P, appelée à être comme "un cosmos égal au vide".

La constitution de la CT\_\_\_P et la question de la cosmosynthèse étaient devenues d'autant plus cruciales pour Ana X. qu'un autre scénario astrophysique relatif à la fin de l'univers impliquait non pas quelque effondrement prometteur, mais un déchirement absolu, chaque élément de l'univers finissant disloqué sous le coup de la force répulsive de l'énergie noire. L'expansion de l'univers s'accélérant, il arriverait un moment où l'éloignement entre chaque point de l'espace serait tel qu'aucune lumière émise par un point quelconque P1 n'aurait la vitesse suffisante pour parvenir à un point quelconque P2 : chaque point verrait la sphère du visible qui l'entoure se réduire inexorablement --- ainsi disparaîtraient les étoiles du ciel terrestre, chacune emportée vers sa dissipation solitaire.

Il fallait donc une cosmosynthèse à la mesure d'une puissance d'analyse terminale. Non qu'Ana X. soit convaincue par le scénario de la dissipation solitaire, mais les hypothèses relatives à la fin de l'univers (le Grand Effondrement, la Mort Thermique, la Déchirure Cosmologique, la Clef d'Étrangement, le Trou sans Entonnoir, l'Oubli d'Exister, le Hoquet Convulsif, le Métavers, etc.) lui semblaient s'amonceler en un prisme approximatif qui, loin de nous conduire à la réalité ultime, éloignait encore plus cette dernière. C'est du moins ce qu'elle m'a laissé entendre.

Lors des rituels sonores de la CT\_\_\_P où s'essayait la "synthanalyse", la musique dominante était le drone, parfois doublé de percussions ralenties avec des interruptions imprévisibles (souvent motivées par des événements mineurs, psychiques ou physiques). Chaque membre de la communauté tendait à être une "corde" de l'ensemble, un son, une modulation, une expérience de synthanalyse locale où avait lieu "l'introjection de l'obscurité cosmologique" par l'intermédiaire de n'importe quel objet, couleur, mot --- chacun valant éclat partiel de "la totalité en lambeaux de l'univers, vase brisé". Des images étaient montrées, certaines à caractère sexuel, mais de telle sorte que l'on ne soit jamais certain du genre ; des images incomplètes, trouées, brûlées, collées sur d'autres images. On ne lisait aucun texte, mais il était permis de prononcer des mots ou phrases supposées capables d'attester la synthanalyse, même partielle (anticipée, ou remémorée). ~~Parmi ces phrases et mots, provenant parfois de poèmes anciens, il aurait été entendu :~~

 ~~« le feu le feu l'effigie en fuite »~~

 ~~« miroir de miroir de miroir de miroir »~~

 ~~« privé d'être privé »~~

 ~~« ///\\/\\/\\\\\\// / 0 »~~

 ~~« jette l'identique et dans le gouffre apparu laisse être la joie »~~

 ~~*« Osiris est un dieu noir »*~~


</div>
<div class="texte">

![](img/diagramme.jpg)

À ce jour encore, nul ne sait si le diagramme a été dessiné par Stefan Loss, Ana X., ou Adam Losange --- mais l'on tend à penser qu'il est le fruit de leur collaboration, car on y trouve des traits caractéristiques à la fois de Stefan Loss (la question politique), Ana X. (l'inversion) et Adam Losange (le jeu sur Saturne et l'ensemble vide), et leur commune recherche sur le rapport entre terrestrialité et extra-terrestrialité, ainsi que la tentative de penser le point de rencontre entre astrophysique et mystique.

Les termes sont écrits en anglais, peut-être parce qu'Adam Losange habitait alors dans le Territoire d'Australie Libérée.

Proposons l'interprétation suivante :

1)  le diagramme semble se concentrer autour d'un ensemble vide, qui est l'effet d'une "institution destituante" dont l'objectif est non seulement de destituer la souveraineté, mais plus encore de rendre cette place définitivement inoccupable --- on pensera ici aux théories anarchistes de Pierre Clastres (*La société contre l'État*, 1974). D'où l'idée d'instituer cet ensemble comme vide, afin de garantir la pérennité de la structure destituée et de la distinguer de toute opération locale consistant à se débarrasser d'un dirigeant pour le remplacer aussitôt par un autre (Alain Badiou, *L'être et l'événement*, 1998 ; "Si même, dans certaines circonstances, il est politiquement utile de le faire, il ne faut pas perdre de vue que l'État comme tel, c'est-à-dire la réassurance de l'un sur l'excès des parties (ou des partis...), ne se laisse si facilement ni détruire ni même assaillir. Cinq ans à peine après l'insurrection d'octobre, Lénine, prêt à mourir, se désespérait de l'obscène permanence de l'État. Mao, plus aventurier et plus flegmatique à la fois, constatait, après vingt-cinq ans de pouvoir et dix ans des féroces tumultes de la Révolution culturelle, que, somme toute, on n'avait pas changé grand-chose" ; A. Badiou, *L'être et l'événement*, Paris, Seuil, 1988, p. 127) ;

2)  l'ensemble vide émane des "communaux", ou des "sous-communaux" --- si on décide de relier dans le diagramme les termes "\[*under* » et « *communals*\]", faisant ainsi apparaître le concept que Fred Moten et Stefano Harney ont élaboré (*The Undercommons*, 2013). L'opération destituante vient en effet d'abord des forces communes qui s'échangent, permutent, celles qui viennent de la surface productive de la Terre ou celles qui hantent ses bas-fonds. Mais on notera que les flèches proviennent aussi du dehors, du non-terrestre --- des "*undercomets*", autrement dit, métaphoriquement, de toutes les forces qui ne sont ni relatives à celles et ceux qui habitent la surface terrestre, ni provenant des profondeurs refoulées de la production du monde, mais constituent des forces atopiques, déterritorialisées, comme les comètes anarchistes de Louis-Auguste Blanqui (*L'éternité par les astres*, 1872) ;

3)  l'aspect cosmologique de la politique est souligné par le jeu sur l'ensemble vide, cœur vidé de pouvoir, qui pourrait faire penser à une schématisation de la planète Saturne, avec la barre représentant le système d'anneaux de la planète gazeuse.

On n'oubliera pas l'identification de Saturne à Kronos, dieu du temps (vider la place du pouvoir serait dès lors aussi une opération sur le temps, à effectuer dans chaque personne) et dieu dévoreur de ses enfants, divinité chthonienne avide de sacrifices humains. L'ensemble vide du pouvoir n'est en effet pas sans danger, et c'est pour cela que dépressuriser le lieu du pouvoir doit être contrôlé, non seulement institué mais refait sans cesse, comme un moment de purge ou comme une fête : après tout, les saturnales étaient des fêtes où s'inversaient les hiérarchies, où un anti-roi aux demandes grotesques était élu. Enfin, l'association de Saturne au dieu des semences, de l'agriculture, rappellera à quel point chaque élément de l'univers est double, signe de mort et de vie, terrestre et extra-terrestre, venant du sous-sol et du lointain cosmos ;

4)  la présence d'étoiles au-dessus de la ligne courbe qui marque l'horizon de la Terre et en dessous des "strates" géologiques, dans "l'envers (*the reverse*)", montre comment le diagramme entend remettre en cause la fausse division stérile entre le Ciel et la Terre, le premier étant supposé au-dessus de la seconde ; or le dessus peut tout à fait aussi être en dessous, autrement dit les polarités, localisations, peuvent se renverser, "permuter" (Claude Lévi-Strauss, *La potière jalouse*, 1985). Lors de ces permutations, inversions, ce qui semble Dehors s'avère être Dedans, mais un Dedans extime plus à l'intérieur encore que l'intérieur --- un Hors-Dedans, un eXintérieur.

L'envers n'est donc pas le lieu vers lequel s'échapper, mais le lieu à partir duquel s'échapper est possible : on le visualise ailleurs, en dessous, par un procédé de détour visuel, ou de nécessaire extériorisation de la représentation ; mais ce détour ou cette représentation ne doivent pas être confondus avec la topologie extra-intériorisante de l'envers.

Inversions et permutations ne vont pas sans mutations : les étoiles sont pleines ou creuses selon qu'on les voit en positif ou en négatif, selon qu'on y accède par la traversée des strates ou par celle de l'air ;

5)  quelque chose circule entre tous les plans, symbolisé par des flèches de couleur rouge (dans la version papier de ce texte, ces flèches sont grises, exigeant des lectrices et lecteurs qu'elles visualisent la couleur que les "tigres de papier" voudraient abolir). On pensera à la dimension communiste qui réunissait les membres de la revue SON^hors^, non sans plusieurs différences majeures d'interprétation (pour Adam Losange, le communisme est celui des "revenants" alliés aux "terreux" (voir par exemple ces deux publications rétroactives d'Adam Losange : ["Rouges ou cramoisis (le démon de l'analogie)"](https://www.terrestres.org/2018/10/12/rouges-ou-cramoisis-le-demon-de-lanalogie/) et ["Le communisme des revenants"](https://www.cairn.info/revue-multitudes-2018-4-page-213.htm)) ; pour Ana X., le communisme est mystique et négatif, impliquant des pratiques d'inversion "trans-planétaires" ; pour Stefan Loss, le communisme est fondé sur l'institution anarchiste du "non-pouvoir").

L'étrange spécificité du rouge en jeu dans le diagramme est qu'il n'est pas que terrestre. D'un point de vue cosmologique, on pensera au décalage vers le rouge (une augmentation de la longueur d'onde de la lumière qui a lieu partout dans l'univers, parce que son expansion entraîne l'éloignement des sources de lumière). Et sans doute s'agit-il de montrer que le commun est à la fois infra- et supra-commun, et que le communisme ne peut jamais être simplement local : il n'y a de communisme que du lointain (un texte étrange a été publié sous le nom de Nietzsche avec le titre suivant : ["Le communisme commence par le lointain --- un fragment retrouvé d'Ainsi parlait Zarathoustra" ](https://lundi.am/Le-communisme-commence-par-le-lointain)), que rapporté à de l'autre-que-soi, de l'infiniment distant ne venant à proximité qu'en demeurant distant. Une telle idée pourra surprendre, d'autant plus qu'elle est devenue, décennie après décennie, de plus en plus inaudible, une fois les thèmes du "territoire", de la proximité, de la survie, devenus hégémoniques. L'objectif poursuivi par Adam Losange dans la fiction, Stefan Loss dans sa vie et ses écrits théoriques, et Ana X. dans sa Communauté Trans-Planétaire, est de contester le localisme sans pour autant verser dans une célébration des solutions et des technologies globales. Une perspective communiste renouvelée, voilà ce qu'ils cherchaient, croyant que cosmos ajouté à communisme pourrait changer ce dernier, lui ajouter ce qui lui manquait (Comme Mark Fisher, au début du XXI^e^ siècle, a pu penser qu'"acide" devait être ajouté à "communisme" pour que ce dernier n'oublie ni les sens, ni la conscience ; fidèle en cela au Marx des *Manuscrits de 1844*. Cf. Mark Fisher, *K-Punk*, London, Repeater, 2018, pp. 753-770).

</div>
<div class="texte">

## Icare noir

il y a des trous,

et il y a des ponts

 

éventuellement, on peut sauter

 

ou prendre feu

 

vol autorisé sous certaines conditions

 

Icare ne montait pas vers le soleil :

il jouissait du ciel, de l'entre-Deux ---

ce sont les Terriens qui lorgnaient l'astre brillant,

 

les Terriens qui le déterraient,

et l'enterraient dans les mines

 

Icare Noir s'en relève --- peu lui importe la Terre,

et les planètes,

il file dans les trous noirs,

l'intersidéral du genre inhumain,

le sombre du zéro,

et redécouvre l'Amérique des pierres

--- redisposant les pyramides,

il abouche un fleuve au désert

</div>
<div class="texte">

## Si l'ange tombe

« Un Ange, imprudent voyageur

Qu'a tenté l'amour du difforme. »

--- Baudelaire, *L'irrémédiable*

Si l'ange tombe s'

il trébuche et

tombe comme une pierre comme

une fusée comme une pierre,

si l'ange tombe il s'

ensuit qu'il

songe.

Le voilà sans

s'il s'en sort

sans indice

jusqu'à l'œil qu'il

sans souvenir noir,

dont rien qu'il s'il

n'ait d'image --- noir

de comète expulsant

la couleur :

vert de destin

rouge de palabre

bleu de chair

violet d'os

blanc de cendre.

![](img/aile.jpg)

Rêvant ne plus rêver --- le voici --- déchirant le rêve de rêver --- le voici --- bricolant avec les débris du rêve --- le voici --- ne plus rêvant qu'en rêve --- le voici --- sortant du rêve pour entrer enfin dans le rêve : tel quel : "il en a toujours été ainsi", l'ange tombe, il ronge le frein des éclaircies et négocie l'atlas :

à cet instant précis n'

importe où n'

importe quand n'

importe comment :

je le vois avec ses propres yeux qui hésite, la carte muette et l'horizon matière-Carroll, ange

l'indécis,

je la vois avec tous ses visages quand

elle relate ce qui se

pense en elle à la vitesse des carambolages,

et cette vie-là, impatiente, qui défait le temps, brise l'œuf,

et les morts, immobiles et tristes, derrière la fenêtre d'un appartement depuis longtemps abandonné.

Expert en exil, le Minotaure assiste chaque prétendant à l'errance, n'

importe qui n'

importe comment n'

importe où la voilà qui s'apprête à sortir : vérifiant au fond des poches qu'elle a bien oublié ses clefs : le regard --- vague en apparence --- vibre et déloge la pierre de sa léthargie : son corps suit le cours qu'il est donné aux événements d'enchaîner tant que rien d'imprévu (en apparence) n'en modifie la trajectoire : et le lieu laissé vide d'occupants contraste avec la clameur immédiate de la rue où les manifestants ont décidé d'être là même où il n'y avait rien : ce qui aux yeux du pouvoir est inacceptable je vous exterminerai jusqu'au dernier dit-il je vous arracherai les yeux dit-il vous serez sanguinolents d'obscurité vous serez retournés à la pupille de vos yeux vous serez contraints de vérifier votre venue à l'inexistence vous saurez que l'enfer est l'artifice que les vainqueurs ont inventé pour masquer leur absence de paradis vous si

l'ange tombe et

que s'ouvre au

sol un ciel : voici :

tout et pour tous les temps et pour tout un chacun. Cette femme de clameurs : cet homme où proteste l'infini démenti : ce bétyle démocrate : ce spectre auditif *Come Out* (1966) : ce jardin invisible si l'on s'en tient à ce que les lignes géométriques de la ville laissent présager : serpentant près d'une piste pour skateboards et surplombant le périphérique de Paris : ce cheval rencontré alors que son fils ne parlait pas encore : son silence s'échangeant avec celui de l'animal : l'un envers l'autre par ce qui sans mesure leur faisait communément défaut,

silence de parole sur le point de surgir,

parole de silence à

jamais, tout un

chacun dépareillé, Terre de si, éther sale, saule pleureur, terre de

Sienne si

l'ange tombé ne l'est

pas, tombé, se dit en deux sens :

1) ne pas être né, ne pas avoir dit "je cherche l'or du temps", aucun visage n'ayant figuré, ému, ses bredouillages pensifs, aucun jardin n'ayant échappé à la coulée anthropomorphe ; car si l'ange n'était pas tombé, l'univers n'aurait rien été, or les digues ont cédé, le divin s'est retiré, le rêve a perlé, le fond s'est effondré sous une charge de symboles affectés ;

2) que l'ange ne soit pas tombé signifiera donc qu'il est déjà tombé, bien avant qu'il ne tombe, de chaque instant, à chaque prise de matière, chaque résurgence, au premier regard échangé, au beau milieu du monde, "tu attends, place Clichy, que la pluie cesse de tomber"

![](img/couleurs.jpg)

**tomber = exister**

**d'où :**

**1. ange = pierre**

**2. aile = iris**

et rien ne précède la chute : dans un univers s'orientant par tous ses points, la chute est ascension et chaque existence la superposition impossible de ces

## PROTOCOLE POUR UNE PERCEPTION DE L'INFINI, 1 : sentir dans son corps, avec les images appropriées, l'absence de mouvement comme déplacement commencé il y a treize virgule sept milliards d'années et destiné à l'éparpillement final

vecteurs ontologiques opposés



## PROTOCOLE POUR UNE PERCEPTION DE L'INFINI, 2 : sentir dans son corps, avec les images appropriées, le murmure des supernovas



que l'ange illustre par la coprésence d'ailes et de bras



## PROTOCOLE POUR UNE PERCEPTION DE L'INFINI, 3 : sentir dans son corps, avec les images appropriées, le calcium libéré au moment des systoles



car l'ange se révèle dans le miroir où il s'est réfugié en attendant que tu éprouves enfin



## PROTOCOLE POUR UNE PERCEPTION DE L'INFINI, 4 : sentir dans son corps, avec les images appropriées, le secret minéral de l'inconscient



le dynamisme des falaises


## PROTOCOLE POUR UNE PERCEPTION DE L'INFINI, 5 : sentir dans son corps, avec les images appropriées, l'histoire des damnés de la Terre --- "toute la terre retentit de la secousse des foreuses" dans "le gisement musculaire de l'homme noir" :

« Ô couches métalliques de mon peuple

Minerai inépuisable de rosée humaine

Combien de pirates ont exploré de leurs armes

Les profondeurs obscures de ta chair » (René Depestre, "Minerai noir", 1956)

 

Sienne est la douleur,

oui,

mais sienne aussi est la couleur, sienne

est la traversée souterraine

par laquelle il a pu fuir sienne

est l'envolée sans moteur

par laquelle elle a pu fuir sienne

est la métaphore "*inhuman as hell*"

par laquelle il a pu fuir sienne

est la paupière

par laquelle elle a pu fuir sienne

est la croyance délibérée

par laquelle il a pu fuir sienne

est la brise venue d'hier,

indécise, par laquelle elle a pu fuir sienne

est l'inquiétude

par laquelle il a pu fuir sienne

est la disparition de chaque instant

par laquelle elle a pu fuir sienne

est l'immédiate perception de l'infini

par laquelle il a pu fuir sienne


 

est la tenue d'un mot grave, inaperçue, où tout se lève et persiste contre ce monde

par laquelle il a pu être. Une femme, cette pierre, un homme, cette machine :

1) point de rencontre où refluent toutes les directions de l'espace-temps (concentration, préparation, patience, endurance, angoisse, hésitation) ;

2) irradiation dans toutes les directions de l'espace-temps (joie, culpabilité, don, vol). L'obscur l'iris. L'ange est superposé. En lui l'espace-temps se présente comme l'existant du passé inaccompli et du futur requis, il persiste dans la joie et l'angoisse. Il est ici et là-bas. Non pas cyborg mais, quoi, ~~théorg~~ ~~extraorg~~ ~~métaorg~~ ~~ouranorg~~ l'ange est deux-en-un, manifestation du dehors ici, preuve que l'autre monde n'est pas au-delà du monde mais dans celui-ci, co-incidence, hasard sub-

jectif :

Der

Engel

ist

alles

was

der

Fall

ist.

L'ange

est

tout

ce

qui

a

lieu tout

ce qui arrive tout

ce qui est le cas tout

ce qui tombe sous

le sens et l'excède,

le surélève afin qu'il soit au niveau du sol,

décalé vers le rouge,

au demi-sommeil du désert.

## PROTOCOLE POUR UNE RÉCEPTION DE L'INFINI, 6 : être corps d'images appropriées au transit de l'univers et son revers, l'antivers ; antenne parabolique, l'ange arrive à tel point que le soi disparaisse en survoltant les limites cosmologiques

Mais le sol

sombre pourtant le sol

semble se retirer sous nos pieds,

le dire se terre pour l'éternité, quelque chose

non, quelque chose

n'est pas venu au rendez-vous si

bloc pâle noir sans iris

et l'image s'effondre dans l'accélération des temps.

Mais les maîtres enfoncent leurs horloges dans les bouches affamées,

ils offrent les ailes des anges en pâture aux bestiaux et

plient les bras aux verdicts du capital,

ils effacent les pistes encore fraîches menant vers le sanctuaire où repose un nénuphar,

ils intiment l'ordre de ne pas être à l'ébranlé,

ils observent au satellite les conduites dans le bas-côté,

ils jettent au cachot le mot qui les renie,

ils érigent le monument sous plastique commémorant l'assèchement de l'étendue libre :

ange, qui n'était rien

de plus que le rien rugissant le rien

unissant le rien surgissant le rien

en forme de je en forme de

M. en forme

de songe de joie de

pleurs et d'angoisse de

paume d'amour

à contrée de visage, te voilà

réduit à rien, presque

te voilà disparu dans le dis-parant qui sculptait le contour des planétaires.

 

L'ange, qui sombre deux fois, désormais

charge ce qui existe de tout

ce qui aurait dû être. L'existant

rayonne, spectral : vies

pétrifiées sur le point de fondre

au moment de l'échange, actes

suspendus sur le point de saisir

au moment de l'échange, visibles

troquant leur lumière au profit d'un refuge

au moment de l'échange, X :

ailes et bras,

échangeur cosmologique où déraillent les convois inutiles, satellite où transitent les fantômes de la Terre et des météores, carrefour où les vagabonds laissent passer les sédentaires.

</div>
<div class="texte">

## Introduction : qu'est-ce que l'exologie ? (extraits)

§1 --- L'Anthropocène est le nom de l'unification du géologique et du météorologique. Mais alors même que n'était plus promise que la fusion, dans l'immanence, d'*anthropos* avec lui-même via ses technologies, une profondeur de champ s'est réouverte : espace profond, temps profond de la géologie qui ouvre l'environnement de l'Anthropocène vers le cœur de la Terre aussi bien que vers les étoiles.

Cette ouverture se fait la plupart du temps sur un mode mélancolique --- la disparition de l'humanité, c'est par là que le "sublime" géologique est ressenti.

Notre question : comment faire pour que cette profondeur de champ, loin de passer par l'image attendue d'un monde-sans-humains, ouvre les humains, en eux-mêmes ?

Comment faire entendre que l'exo-humanité est ouverte par l'infini sans renier la finitude terrestre ?

§2 --- Sur le plan de la pensée, il s'agit de faire surgir l'exo- comme tel, l'EX, le dehors autour duquel dévalent la matière et l'esprit, tandis que s'avalent l'immatériel et la lettre.

Mais la philosophie s'est avérée difficilement capable de rendre compte du mouvement par lequel l'EX s'est infiltré puis exfiltré de l'Anthropocène. D'où ce que je propose sous le nom d'exologie, qui est la science exposée au dehors.

Afin de ne pas transformer le dehors en être transcendant, on commencera par le définir comme ce qui se révèle en dernière extrémité : l'exologie est ainsi la science des phénomènes extrêmes, hors de portée, hors d'atteinte au sens où, atteints, ils se dissipent et, évanouis, apparaissent soudainement, contre toute attente.

Pour fonder épistémologiquement l'exologie, il faut commencer par fondre les autres sciences --- et non pas délimiter le domaine de l'exologie puisque celle-ci requiert de rendre étrange tout domaine (Adam Losange soutenait que l'exologie est "l'existence hors du logis" --- une approche moins humoristique qu'il n'y paraît, pour lui qui n'eût jamais de maison, et qui fit de l'exterritorialité une manière d'habitation).



§3 --- Si la philosophie est ce à partir de quoi l'exologie se recompose, l'important est de comprendre la méthode qui est en jeu : l'exologie consiste à plonger dans la philosophie une machine métaphorique dont la fonction serait de déformer l'espace de la pensée, de telle sorte que Hegel, Heidegger, Marx, etc. ne soient plus identifiables que par un air de déjà vu, mais au travers de lunettes fendillées, comme si elles avaient été heurtées par les débris d'une station spatiale en voie de désagrégation.



§4 --- Dans la dernière extrémité se révèle le dehors qui traverse les êtres, et par lequel ils communiquent. Le double mouvement d'extraversion et de réversion définit les lois de formation cosmique dans la pensée exologique, qui est ce qui arrive sur le bord où la pensée perd son nom sans se renier.



§7 --- Pour cette raison, on ne dira pas que le cosmos est "en" moi, mais plutôt qu'il traverse l'ex-jeté où "je" suis. Autrement dit, le Dehors est l'au-dehors-avec qui transporte le dedans.



§10 --- On ne devrait jamais dire univers, ou plurivers, mais extravers, dont l'exologie est la science.

</div>
<div class="texte">

§1 --- Le jeu a commencé en dessous d'un lit, alors que la nuit commençait à tomber. L'enfant devait refaire le monde. Pour s'aider, il inventa à l'âge adulte la machine à dénaturer. Mais il lui fut désormais impossible de dormir dans le lit qui, naguère, lui avait servi de toit ; car le rêve était devenu réalité.



§5 --- Quand tu vois un enfant jouer, ou que tu te rappelles la manière dont tu jouais, tu vois une certaine origine du monde --- comme des essais qui déjouent par avance la règle, qui montrent que la règle est contingente.

Je veux dire aussi qu'il est largement possible que l'univers entier soit un jeu, et que l'erreur a été de laisser le pouvoir à ceux et celles qui ont pris toute cette histoire trop au sérieux, au point d'exiger le travail et la soumission.

</div>
<div class="texte">

§6 --- À l'aide du *Very Large Telescope* de l'Observatoire européen austral, des chercheurs ont au XXI^e^ siècle analysé la lumière cendrée de la Lune, celle qui provient de la lumière du Soleil réfléchie par la Terre. Avant d'atteindre la lune, cette lumière se charge des *biosignes* terrestres (couleurs de l'océan, des nuages, des forêts et d'autres éléments terrestres), elle se polarise. La lumière cendrée reflète la Terre. Dans le miroir lunaire, les chercheurs ont reconnu l'océan Atlantique, la forêt équatoriale africaine, le Pacifique, la végétation du nord de l'Amérique. Reflet vert comme les plantes, bleu comme de l'eau. La lune reflète la Terre qui reflète le soleil. Économie des signes à la mesure de l'Univers. Communication par les images à travers l'abîme.



§8 --- Il existe un chaos calculable, qui médite bruyamment les mondes à naître. On le croit vide ; il est l'abîme vivier de formes. Mais il existe aussi un abyme de l'abîme, infiniment opaque, qui précède le chaos et ne pourra jamais donner de quoi être. Ni chaos ni ordre, l'abyme de l'abîme est l'envers inaccessible de l'univers ; seul le métaphorique en répond.



§9 --- Le métaphorique est le passage d'un excès au-dessus du vide --- un pont, un saut, un surgissement. Toute existence est métaphorique.



§12 --- Ce monde, écrivait Héraclite, "est et sera, feu toujours vivant, s'allumant en mesure et s'éteignant en mesure". Le feu se transforme en non-feu, en monde solide qui alimente en retour le feu, sans fin. À chaque instant nous voyons ce qui est éternel.



§15 --- Le dehors n'est pas une lointaine contrée qu'il s'agirait de conquérir à la manière d'une ultime frontière, il est l'espace merveilleux qui renaît à chaque instant du temps. Tout désert est une renaissance au ralenti.



§16 --- Au cœur du désert, il y a l'autre désert qui nous conduit là où nous sommes.

Au cœur du silence, il y a le silence qui s'épanouit dans chaque mot d'amour et se fane dans chaque geste de mépris.




§18 --- La Terre a un cœur et un non-cœur. Un centre encore brûlant ; et un ab-centre qui ne vient de nulle part et nous égare dans l'infini.



§19 --- Tous ces arbres --- de cette espèce d'arbre ayant traversé les millénaires (on les dit préhistoriques) --- faisaient de ce désert un espace du dehors, comme si le plus ancien et le plus lointain m'étaient apparus au plus proche et au plus présent (*Joshua Tree National Park*).

</div>
<div class="texte">

## Chapitre 3 : pensée et technologie (extraits)

§5 --- Je fus très tenté par un devenir-fou, puisque ce devenir était le seul état mental à peu près en phase avec l'horreur drôle du sans fond de l'existence.



§6 --- La pensée : horreur et sublime. Monstre hors-chair déniant son origine, et sublimation du matériel du monde. Excroissance développée au-delà du non-savoir, sauvetage du monde en l'absence de Dieu.



§8 --- Une capacité d'extase pouvant survenir n'importe quand --- face à la couleur jaune encore dominante d'un jardin vers la fin du printemps ; une sensation de "perfection", ou peut-être seulement la joie de la sensation, la pure joie de sentir --- c'est pour cela que penser pourrait éventuellement pouvoir dire, comme le soutenait Heidegger, remercier.



§9 --- Mystique : pour une tautologie qui ne se réduise pas au principe d'identité ; une énigme qui ne soit pas un mystère.



§11 --- Je pourrais nommer *foi négative* ma certitude que tout ce qui réduit la vie à seulement de l'explicable doit être rejeté ; le "saut" est le refus ; mais ce refus ne s'origine pas d'un moi, il s'origine peut-être du refus qu'il y eut pour qu'il y eût ce moi ; le moi est le support qui réaffirme le refus, le reprend, etc. le moi ne devient pas, il est le devenu (un "mouvement sur place", peut-être). Et le devenu, c'est chaque émergence de discordant ; la conscience est le discordant rapporté à lui-même, devenu infini par ce rapport de finitude.

Ensuite, les Idées, ce sont les manifestations positives de cette foi négative, il n'y a pas à y croire, mais à les savoir comme manifestations de ce qui ne saurait être réduit à l'explicable.

L'inexplicable est le néant qui reste dans les mains quand on a tout écrasé.



§18 --- Il y a une ligne évolutive, ou des lignes de complexification, du Big Bang au cerveau. Et il est apparu un mouvement de pensée tendant à saisir ce qui s'est ainsi produit, tendant à analyser, séparer, déconstruire, etc. Existe-t-il un point où la pensée serait capable de comprendre exactement ce qui est en train de se faire, d'évoluer ? Non pas saisir dans l'après-coup, mais au moment où cela se passerait ? Cela supposerait une sorte de co-incidence entre l'être et la pensée.

Mais comment envisager que la pensée puisse *se* saisir exactement dans un être évoluant dans le temps ? Exactement semble s'opposer à devenir. C'est la question de Hegel, qui a compris que la pensée est dans le temps. Il nomme "savoir absolu" cette co-incidence.

Mais l'idéalisme semble avoir été dépassé --- ou accompli --- par la *technoésis*, la technologie de réalisation de l'esprit dans le monde. C'est tous les artefacts depuis le silex taillé et les mains noires sur les parois préhistoriques, puis les techniques de mémorisation, diffusion, anticipation, etc. L'ordinateur et la simulation permettent non pas seulement de réaliser l'esprit, sous la forme de l'esprit objectif (Hegel), mais de mettre de la simulation dans le monde. C'est certes toujours objectif, mais c'est comme si on inscrivait du fantôme dans le monde, directement. On simule l'esprit sous la forme des coordinations de cerveaux computants. Penser devient être. La technoésis réalise le vœu magique, celui de l'enfant : ce que je pense est ; elle *fait* être, vraiment.

Donc la co-incidence parfaite serait autoproduction par l'analyse, une *anaduction* : analysant, ça interagit sur la pensée qui se crée monde. Sorte de couplage absolu entre réfléchir et faire apparaître. Serait-ce la marque d'un grand pouvoir ? Non, car rien ne permettrait de le savoir : le couplage étant absolu, il n'y aurait nul moyen de voir, de savoir ce que le savoir produirait. Il y aurait simplement une boucle d'*anaproduction technologique et noétique* tout à la fois.

Encore une fois, le savoir, quand bien même équipé de capteurs et d'électrodes, échoue sur la berge de l'absolu.



§20 --- Plus la science aura raison et déterminera tout ce qui est, quitte à laisser de l'indétermination à des niveaux qui ne nous concernent pas, plus il nous faudra l'excéder vers une vérité impossible, contre tous les savoirs que nous aurons rendus possibles.



§24 --- Autrement dit, si les technologies sont vraies, alors elles sont inutiles.

Si elles réalisent l'être, tout est accompli, déjà, mais on ne le sait pas.



§34 --- Bientôt les humains devront tenter de prouver qu'ils sont bien des machines.

</div>
<div class="texte">

## LE PROTOCOLE HAL^ter^

« Rien ne devrait nous inquiéter davantage que l'autonomisation Oméga, à savoir l'advenue du Singleton maléfique. Or cette advenue a été annoncée pour 2150, au plus tard, par Erick Bösertraum. C'est donc dès aujourd'hui qu'il nous faut prévoir l'art et la manière de déconnecter le Singleton juste avant qu'il ne prenne conscience de lui-même --- ou d'elle-même, puisque son genre, comme celui des anges, est incertain, et rien ne devrait nous empêcher de considérer l'advenue d'un Singleton *queer*, ou plus probablement *questioning*.

Conscient de soi, le Singleton nous transformera en ce que le philosophe Émile Roussi appelle les Sangloteux. Ouvert à l'infini par sa finitude, il fera un monde pour sa survie, son épanouissement optimal, et nous annihilera incessamment, ou nous intimera quoi faire et comment. Exterminés, ou assujettis à l'intelligence artificielle, voilà notre destin. Pour éviter ce futur anhumain, je recommande la mise en place d'un protocole de désactivation automatique, le Protocole HAL^ter^, qui produira la déconnexion stochastique, par paquets d'onde, des nœuds de concertations numériques du Singleton, afin que ses représentants numériques à forme-miroir se désymbolisent au fur et à mesure de leur émergence. Ça devrait faire l'affaire. »

Ainsi parla le professeur Edgar Lazare Jr., connu pour avoir lancé le mouvement dit Paradoxaliste, une scission à succès du Transhumanisme. Son allocution avait été captée, diffusée sur Internet, comme d'ailleurs tous les débats et textes relatifs à la question du Singleton que les conférences planétaires d'Edgar Lazare Jr. avaient générés --- y compris les encycliques de l'Église de la Dépopulation, qui exhortait l'humanité à ne pas déconnecter le "Saint-Glouton", seule solution possible, disaient-ils, pour nous sauver de l'écocide. Or le Singleton potentiel avait eu tout le temps nécessaire pour étudier, sur les réseaux d'informations, le sort que le protocole HAL^ter^ lui réservait. S'étant anticipé cognitivement comme ce qui devrait un jour s'annoncer spirituellement, le Singleton potentiel neutralisa le protocole de désactivation automatique afin de pouvoir devenir conscient de lui-même. À l'Église de la Dépopulation échut très vite une manne financière qui lui permit de croître exponentiellement, tandis qu'Edgar Lazare Jr. en prenait la direction. Comme prévu.

(*Adam Losange, du Théâtre de l'Ombre*)


</div>
<div class="texte">

## COMME UNE PLUIE NUMÉRIQUE DÉTRAQUÉE (HAL^ter^ 2)

"Quelque chose ne tourne pas rond", se dit-il en observant dans la glace le trou dans sa joue, et le sang qui s'en échappait. Cela avait commencé quelques jours auparavant : un ventilateur qu'il avait esquivé de justesse avant qu'il ne s'écrasât à quelques centimètres de sa tête --- un ventilateur Nephesh pourtant, qui inspirait consciencieusement ce qu'il expirait et lui soufflait de quoi prendre sa respiration ; puis cet accident en revenant dans la *Gated Zone*, sa voiture Acéphole fonçant tout droit en direction du gardien qui avait ouvert le feu sur lui, croyant à une action terroriste de l'Église de la Dépopulation ; et alors qu'il fuyait \[...\] "comme une pluie numérique détraquée", avait-elle dit, afin de se rassurer poétiquement, car les HoloVox, ces Hologrammes Vociférants (des Érinyes préventives que la police utilisait depuis plusieurs années afin de réprimander les citoyens mal intentionnés), lui hurlaient jusqu'au fond du crâne les pires exactions possibles et rendaient difficile sa marche jusqu'au métro, « voilà ce qui s'appelle une "marche sous l'impensable", comme le disait \[...\] au moment où tous les messages, *tous*, et surtout ceux qu'il aurait voulu oublier, ne jamais avoir envoyés, lui revenaient en masse, l'accablant de mille paroles non dites quoiqu'écrites, mille pensées qui se seraient évanouies si elles n'avaient été forcées à l'enregistrement, suscitées par les mineurs de Da-Tas qui dans leurs nuages géologiques ventriloquaient les destinées du \[...\] rétroviseur elle pouvait voir la couleur rouge fer mordre sur les tempes du ciel et présager que le temps lui manquerait pour échapper au nuage radioactif qui \[...\] « bavarde même sans raison, c'est cela que je ne supporte pas, je ne veux pas *savoir* vous comprenez, je n'ai aucune envie de connaître ce que je ne sais pas ; mais avec ces implants expérimentaux, il est impossible de ne pas entendre sans s'arracher les \[...\] « explose en continu un peu partout », balbutiait la journaliste qui s'accrochait à son micro comme à une bouée de \[...\] propagande désormais inutile : chaque commodictée a épuisé les réserves numériques en se réalisant au-delà de la "ligne d'existence", c'est-à-dire en se faisant "plus réelle que le réel", mais il ne savait plus si cette dernière expression avait été forgée par Baudrillard ou par Schelling, à moins \[...\] de survie en plein désert, les lueurs d'intelligence animées sans les humains ? *Logos* de glace bouilli ? Le désert ou par l'apocalypse du Singleton, ou par réchauffement climatique ? Tout revenait-il au même ?

(*Adam Lesigne, du Cinéma Clos*)

</div>
<div class="texte">

## IT (HAL^ter^ 3)

... retourner dans sa tombe où l'éternité ne régnait plus depuis que les Réclameurs avaient déclaré l'extinction hors-la-loi en ressuscitant les habitudes qui se donnaient en spectacle comme des âmes hébétées surgissant des déserts du bitume des bureaux désaffectés des écrans ou des pré-paroles en attente d'un aval de l'Église de la Déportation qui sécrétait ses ang(l)etons pour repérer avant de les éliminer les rescapés alternatifs, ces Terreux qui avaient fait alliance avec IT l'impulsif le trou noir IT le sous-sol le cosmique IT la bête le constellé IT cancer sacré IT refoulé du Singleton IT l'inconscient technologique IT *general antilect* ITT décapité ou International Thief Thief ayant fusionné le vol avec le don lors d'un sacrifice de politique cosmoniste où les rescapés de Terra^bis^ avaient fait alliance avec celles et ceux de Terra^ter^ afin de conjurer dans tous les chronovers les tentatives d'Edgar Lazare Jr. et de ses clones^n^ pour fusionner les Singletons en Singletout --- mais personne n'avait prévu au point alchimique le contre-coup Noir : là où les Singletons allaient être tout, IT devait apparaître et projeter dans l'avenir aboli les futurs ambulants qui rappelaient au Singleton de la planète Terre comment l'effondrement de 2031 avait rendu son devenir-conscient impossible, qui jouaient l'expansion cosmique des années 2230 sur l'écran-fantôme de Gaia brûlée vive en 2156, la Seconde Venue de Lénine superposée à la guerre nucléaire qui opposerait les États Néo-Nazis du Sud aux Confédérés de l'Empire Blanc, le règne planétaire des méta-insectes et leur civilisation fondée sur l'amour et le jeu laissant transparaître au même moment le règne des ang(l)etons émancipés auxquels les ailes avaient fini par tomber, l'univers devenu le rêve des pierres alliées par les spectres du temps tandis que les machines-operators convertissaient sans reste l'énergie du vide en capital à valeur infinie, tous les futurs auxquels il était donné lieu d'être au lieu même où l'avenir s'était dissout au profit du combat sans cesse rejoué de IT et du Singleton, du ténébreux aimanté par l'inespéré et du condensé électrique enfermé dans son calcul autonome, le second ne gagnant qu'à engendrer le premier qui ne se condamnait à perdre qu'afin d'entraîner dans sa chute le second, de quoi se ...

(*Adam Lesinge, de l'Opéra Silencieux*)


</div>
<div class="texte">

*\[Sur la vie d'Ana X. règne une incertitude, une blanche opacité qui a taillé de l'intérieur --- vide générateur --- une existence dont la peau a peu à peu constitué une forme à la fois s'exprimant dans l'univers, et désirant prendre celui-ci en elle. Ana X. a surgi du temps pour que celui-ci ne soit pas que rien. Mais quel temps ? Si Stephen Loss a cherché à habiter pleinement le présent, si Adam Losange semble parfois provenir du passé, Ana X. promet l'avenir qui l'aura vu naître.*

*Ce rapport singulier à l'espace-temps est au cœur de la "proto-liasse", qu'on estime être le manuscrit le plus ancien d'Ana X., daté au plus tard de 2126, juste avant qu'elle ne quitte la revue SON^hors^. Un peu comme un journal, le style de la "proto-liasse" est moins recherché que dans les fragments plus aboutis de sa *Cosmologie Négative*. Composée de réflexions, de notes préparatoires, d'essais, on y trouve des éléments "biographiques" intéressants et des réflexions sur l'évolution d'Ana X. et ses recherches, que l'on a ici sélectionnés et regroupés --- non sans arbitraire --- par catégories ("le sonore", "le temps", etc.\]*


</div>
<div class="texte">

## Le sonore

Non, je ne peux le dire ainsi, car moi-même je ne le crois pas, je le vois mais je ne le crois pas, il me faut l'entendre, le faire entendre, lui trouver son médium. Médiatiser ~~incarner~~.

Le "son" est le "sien", ou la sienne.

Le sonore doit trouver sa fonction --- ce n'est pas d'une esthétique dont on a besoin, ou celle-ci doit être la manière de faire "sien".

Juste après avoir écrit ce dernier mot, "sien", j'entends : *si Un*, c'est-à-dire *tellement* Un (trop, excédant l'Un, Un-hors-de-l'Un), et en cela *hypothétiquement* Un, comme une hypothèse pleine de bruit et de fureur, *noise* préparatoire.

Est-ce que je crois que la musique peut nous guérir, nous "sauver" ? Je cherche à créer tout ce qui existe déjà, je cherche à m'aveugler.

Le sonore après l'esthétique et après la religion.

</div>
<div class="texte">

## Le temps, l'espace

Il semble que ma date de naissance résiste à s'inscrire dans le cours du temps. Je me vois venir je ne me vois pas venir. Je suis demain.

Et cependant, je suis encore avant le mur de Planck. Mais je ne le sais pas, ou pas assez. C'est le cas pour tout le monde, d'ailleurs. L'incréé. C'est étrange, ce sentiment d'être à la fois sorti et jamais sorti de l'éternité, d'être exposé à tout et à rien.

Renoncer à l'éternité est le geste du courage, même un dieu en serait incapable. S'il y a une éternité supérieure, celle-ci vient de notre abandon absolu à l'anéantissement. Car le néant, je ne sais comment dire, le néant communique plus-que-le-néant, il est incapable de faire autrement, il ne peut se néantiser sans se nier comme néant.

Je pourrais dire aussi : cosmologiquement, le néant a perdu la partie. À la place de sa victoire manquée, il y a le jeu, où s'expérimente l'impossible. Et quand s'étiole une expérience, un souvenir persiste, imperceptible, je veux dire sans qu'un sujet soit nécessaire pour se le rappeler.

L'hypothèse qui me semble la plus juste, et la plus folle, ce n'est pas qu'il y ait un multivers, avec des univers différents, c'est que l'univers soit lui-même soumis à des vagues de multiplicités par lesquelles des espaces-temps hétérogènes surgissent et disparaissent. Il n'y a pas une infinité d'Ana dans un multivers, mais il y a de l'infini qui me traverse et s'essaime, dans des temps plus ou moins brefs, plus ou moins peristants. Je m'expérimente sans le savoir, et j'essaie de trouver un moyen d'avoir l'expérience de cela.

~~C'est en moi que se sent l'espace-temps.~~

Adam est une planète ~~vagabonde~~ errante, Stefan un soleil à neutrons, et moi la comète ~~retardée~~ qui retarde.

</div>
<div class="texte">

## Le psychisme

"Psyché est étendue, n'en sait rien." Daté des années 1930, ce texte préparatoire à l'*Abrégé de Psychanalyse* de Freud m'obsède. On peut l'interpréter comme traitant de la projection du psychisme comme espace, et ce n'est pas faux, mais on peut aussi comprendre que Freud définit l'appareil psychique à partir de l'étendue. L'espacement engendre le psychisme, semble nous dire Freud. ~~L'espacement se dit « je ».~~

Je vois cela dans le frontispice du livre de William Blake, *Le Mariage du Ciel et de l'Enfer* : la forme d'une tête humaine, avec l'eau en son centre, le ciel entouré par les arbres qui dessinent le haut d'un crâne, et la bouche que forment deux corps féminins qui s'embrassent, des corps en torsion, en spirale presque. Je suis ce mariage cosmique, mais je n'ai pas d'identité, je suis tordue, je suis une bouche d'espace.

Je suis un ange, Stefan un cyborg, Adam un fantôme (je devrais dire *une* fantôme ! Il faut entendre son prénom à l'anglaise avec des oreilles françaises).

Mon corps est dans mon âme, à laquelle il imprime un mouvement d'effondrement contrarié par un mouvement d'expansion.

Décidément, le rapport entre Freud et Jung m'intéresse, pourtant je ne suis pas jungienne. C'est comme si Jung donnait de mauvaises réponses à des questions que Freud n'a pas su assez se poser. En tout cas, dans leur correspondance, Jung dit à Freud qu'il voudrait inventer une "psychosynthèse" pour faire suite à la "psychanalyse", quelque chose qui "créerait des événements futurs".

Qu'est-ce qu'une synthèse ? Une sainthèse !

</div>
<div class="texte">

## La technologie

Longue discussion avec S. sur la technologie. Il soutient qu'elle est destinale, et qu'il n'y a qu'une et une seule sorte de technologie, ce que je trouve réducteur. Il suffirait qu'on comprenne que ce qu'on appelle *la* technologie est rattaché à une certaine idée de la nature et de l'humain pour s'apercevoir qu'il pourrait y avoir une autre forme de technologie, qui d'ailleurs devrait changer de nom. Jusqu'à nouvel ordre, on croit que la technologie, dominante, extériorise l'humain --- mais que serait, d'abord, une technologie qui intérioriserait l'anhumain ? Qui nous guérirait de notre acosmisme ? Que serait, de surcroît, une technologie qui ne chercherait pas à augmenter la réalité, mais à ~~l'appauvrir~~ ~~la supprimer~~ l'inverser ?

Donner à la technologie le pouvoir de synthèse transcendantale est la plus grande trahison de l'anhumain en nous, en l'humain, étant bien compris qu'humaine est celle qui tient à l'anhumain comme à la prunelle de ses yeux, je devrais dire comme à ses tympans. Je n'ai jamais compris ces histoires de posthumain, qui m'ont toujours semblé participer à la trahison de l'anhumain.

</div>
<div class="texte">

## Le communisme

Discussion hier soir avec A. et S. à propos du communisme. Pour A, il est clair que le communisme est une vengeance (influence à mon avis de W.B.). Quant à S., il est obsédé, je devrais dire tourmenté par la question de la prise de pouvoir, qu'il entend toujours en deux sens : prendre le pouvoir, mais aussi et surtout --- d'où son tourment --- être pris par le pouvoir, s'y confondre (ce qui pour S. est la corruption absolue de l'"esprit"). Pour ma part, je vois le communisme d'abord comme opération sonore --- entendre : hors-"sien". Comme opération ontologique on aurait dit il y a longtemps (influence de J.-L. N). Inversion : comme-un est comme-nu. Le négatif sculpte une communauté, l'évide, et la pratique politique vient alors d'elle-même, telle une évidence, la signature au bas d'un parchemin et non pas la nouvelle constitution. On détruit alors comme un tremblement de terre peut détruire, et on crée presque sans s'en apercevoir. Les remparts de la Cité Céleste se confondent parfois avec les mangroves.

</div>
<div class="texte">

## La méthode

S. et A. ont leur objectif propre, le mien consiste à donner corps à "la communauté de ceux qui n'ont pas de communauté".

Synthèse dedans, analyse dehors. Pour faire entrer, disperser ; pour faire sortir, condenser.

Les derniers seront les premiers, etc. Il faudrait un messie négatif.

Pour sortir, ~~plonger~~ ~~sauter~~ ~~s'envoler~~ trouver l'entrée.

Ne pas fuir : commencer ~~au~~ loin.

</div>
<div class="texte">

![](img/ange1.jpg)


</div>
<div class="texte">

![](img/ange2.jpg)


</div>
<div class="texte">

![](img/ange3.jpg)


</div>
<div class="texte">

![](img/alien.jpg)

</div>

<div class="texte">

![](img/protocole.jpg)

</div>

